export class CounterProduct extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__quantityCount = new ObservedPropertySimplePU(1, this, "quantityCount");
        this.__disabled = new ObservedPropertySimplePU(true, this, "disabled");
        this.count = 1;
        this.counterMin = 1;
        this.onNumberChange = () => { };
        this.setInitiallyProvidedValue(params);
        this.declareWatch("quantityCount", this.onChange);
    }
    setInitiallyProvidedValue(params) {
        if (params.quantityCount !== undefined) {
            this.quantityCount = params.quantityCount;
        }
        if (params.disabled !== undefined) {
            this.disabled = params.disabled;
        }
        if (params.count !== undefined) {
            this.count = params.count;
        }
        if (params.counterMin !== undefined) {
            this.counterMin = params.counterMin;
        }
        if (params.onNumberChange !== undefined) {
            this.onNumberChange = params.onNumberChange;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__quantityCount.purgeDependencyOnElmtId(rmElmtId);
        this.__disabled.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__quantityCount.aboutToBeDeleted();
        this.__disabled.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get quantityCount() {
        return this.__quantityCount.get();
    }
    set quantityCount(newValue) {
        this.__quantityCount.set(newValue);
    }
    get disabled() {
        return this.__disabled.get();
    }
    set disabled(newValue) {
        this.__disabled.set(newValue);
    }
    aboutToAppear() {
        this.quantityCount = this.count;
        if (this.quantityCount === this.counterMin) {
            this.disabled = true;
        }
    }
    onChange() {
        this.disabled = (this.quantityCount === this.counterMin);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("../../../../../common/src/main/ets/components/CounterProduct.ets(36:5)");
            Row.width({ "id": 134217924, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Row.height({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create(this.disabled ? { "id": 134217895, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } : { "id": 134217899, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../common/src/main/ets/components/CounterProduct.ets(37:7)");
            Image.width({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.height({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.onClick(() => {
                if (this.disabled) {
                    return;
                }
                this.quantityCount = Math.max(this.quantityCount - 1, this.counterMin);
                this.onNumberChange(this.quantityCount);
            });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`${this.quantityCount}`);
            Text.debugLine("../../../../../common/src/main/ets/components/CounterProduct.ets(47:7)");
            Text.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor(Color.Black);
            Text.textAlign(TextAlign.Center);
            Text.width({ "id": 134217923, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.height({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217913, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../common/src/main/ets/components/CounterProduct.ets(53:7)");
            Image.width({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.height({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.onClick(() => {
                this.quantityCount = Math.min(this.quantityCount + 1, Infinity);
                this.onNumberChange(this.quantityCount);
            });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=CounterProduct.js.map