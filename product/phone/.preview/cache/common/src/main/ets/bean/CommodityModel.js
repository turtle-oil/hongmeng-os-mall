export class Commodity {
    constructor(props) {
        this.id = '';
        this.title = '';
        this.promotion = '';
        this.description = '';
        this.images = [];
        this.detail = [];
        this.price = 0;
        this.specifications = [];
        if (!props) {
            return;
        }
        this.id = props.id;
        this.title = props.title;
        this.promotion = props.promotion;
        this.description = props.description;
        this.images = props.images;
        this.detail = props.detail;
        this.price = props.price;
        this.specifications = props.specifications;
    }
}
//# sourceMappingURL=CommodityModel.js.map