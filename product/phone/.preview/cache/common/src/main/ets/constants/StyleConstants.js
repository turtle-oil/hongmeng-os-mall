/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Constants for common style.
 */
export class StyleConstants {
}
/**
 * Component width percentage: 100%.
 */
StyleConstants.FULL_WIDTH = '100%';
/**
 * Component height percentage: 100%.
 */
StyleConstants.FULL_HEIGHT = '100%';
/**
 * Component height percentage: 70%.
 */
StyleConstants.SEVENTY_HEIGHT = '70%';
/**
 * Component height percentage: 60%.
 */
StyleConstants.SIXTY_HEIGHT = '60%';
/**
 * Component width percentage: 60%.
 */
StyleConstants.SIXTY_WIDTH = '60%';
/**
 * Component height percentage: 50%.
 */
StyleConstants.FIFTY_HEIGHT = '50%';
/**
 * Component height percentage: 50%.
 */
StyleConstants.HUNDRED_FIFTEEN_WIDTH = '115%';
/**
 * Component space vp : 4.
 */
StyleConstants.FOUR_SPACE = '4vp';
/**
 * Component space vp : 12.
 */
StyleConstants.TWELVE_SPACE = '12vp';
/**
 * Component space vp : 14.
 */
StyleConstants.ITEM_SPACE = '14vp';
/**
 * Component space vp : 15.
 */
StyleConstants.FIFTEEN_SPACE = '15vp';
/**
 * Font weight value: 700.
 */
StyleConstants.FONT_WEIGHT_SEVEN = 700;
/**
 * Font weight value: 500.
 */
StyleConstants.FONT_WEIGHT_FIVE = 500;
/**
 * Font weight value: 400.
 */
StyleConstants.FONT_WEIGHT_FOUR = 400;
/**
 * Text line value: 2.
 */
StyleConstants.TWO_TEXT_LINE = 2;
/**
 * Component opacity value: 1.
 */
StyleConstants.FULL_OPACITY = 1;
/**
 * Component opacity value: 0.6.
 */
StyleConstants.SIXTY_OPACITY = 0.6;
/**
 * Component opacity value: 0.8.
 */
StyleConstants.EIGHTY_OPACITY = 0.8;
/**
 * Component layout value: 1.
 */
StyleConstants.LAYOUT_WEIGHT = 1;
/**
 * Flex basic value: 1.
 */
StyleConstants.FLEX_BASIC = 1;
/**
 * Flex shrink value: 1.
 */
StyleConstants.FLEX_SHRINK = 1;
/**
 * Flex grow value: 1.
 */
StyleConstants.FLEX_GROW = 1;
/**
 * Swiper or list display count value: 1.
 */
StyleConstants.DISPLAY_ONE = 1;
/**
 * Swiper or list display count value: 2.
 */
StyleConstants.DISPLAY_TWO = 2;
/**
 * Swiper or list display count value: 3.
 */
StyleConstants.DISPLAY_THREE = 3;
/**
 * Swiper or list display count value: 4.
 */
StyleConstants.DISPLAY_FOUR = 4;
/**
 * Image aspect ratio value: 2.23.
 */
StyleConstants.IMAGE_ASPECT_RATIO = 2.25;
/**
 * Number of value: 0.5.
 */
StyleConstants.HALF_ONE = 0.5;
/**
 * Number of value: -1.
 */
StyleConstants.MINUS_ONE = -1;
//# sourceMappingURL=StyleConstants.js.map