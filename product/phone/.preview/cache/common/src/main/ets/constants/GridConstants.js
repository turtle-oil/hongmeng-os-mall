/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Constants for Grid components.
 */
export class GridConstants {
}
/**
 * Current component width: 4 grids.
 */
GridConstants.COLUMN_FOUR = 4;
/**
 * Current component width: 8 grids.
 */
GridConstants.COLUMN_EIGHT = 8;
/**
 * Current component width: 12 grids.
 */
GridConstants.COLUMN_TWELVE = 12;
/**
 * Current component width: 1 grids.
 */
GridConstants.SPAN_ONE = 1;
/**
 * Current component width: 2 grids.
 */
GridConstants.SPAN_TWO = 2;
/**
 * Current component width: 3 grids.
 */
GridConstants.SPAN_THREE = 3;
/**
 * Current component width: 4 grids.
 */
GridConstants.SPAN_FOUR = 4;
/**
 * Current component width: 8 grids.
 */
GridConstants.SPAN_EIGHT = 8;
/**
 * Current component width: 12 grids.
 */
GridConstants.SPAN_TWELVE = 12;
/**
 * Current component offset 2 grids.
 */
GridConstants.OFFSET_TWO = 2;
/**
 * Current component offset 3 grids.
 */
GridConstants.OFFSET_THREE = 3;
/**
 * Current component offset 6 grids.
 */
GridConstants.OFFSET_SIX = 6;
/**
 * Current component offset 8 grids.
 */
GridConstants.OFFSET_EIGHT = 8;
/**
 * Current component gutter 12.
 */
GridConstants.GUTTER_TWELVE = 12;
//# sourceMappingURL=GridConstants.js.map