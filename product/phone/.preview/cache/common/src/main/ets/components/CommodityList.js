import { EmptyComponent } from '@bundle:com.example.multishopping/phone@common/ets/components/EmptyComponent';
import { StyleConstants } from '@bundle:com.example.multishopping/phone@common/ets/constants/StyleConstants';
import { CommonDataSource } from '@bundle:com.example.multishopping/phone@common/ets/utils/CommonDataSource';
export class CommodityList extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__commodityList = new SynchedPropertyObjectTwoWayPU(params.commodityList, this, "commodityList");
        this.__column = new SynchedPropertySimpleOneWayPU(params.column, this, "column");
        this.onClickItem = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.onClickItem !== undefined) {
            this.onClickItem = params.onClickItem;
        }
    }
    updateStateVars(params) {
        this.__column.reset(params.column);
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__commodityList.purgeDependencyOnElmtId(rmElmtId);
        this.__column.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__commodityList.aboutToBeDeleted();
        this.__column.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get commodityList() {
        return this.__commodityList.get();
    }
    set commodityList(newValue) {
        this.__commodityList.set(newValue);
    }
    get column() {
        return this.__column.get();
    }
    set column(newValue) {
        this.__column.set(newValue);
    }
    CommodityItem(info, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../common/src/main/ets/components/CommodityList.ets(31:5)");
            Column.padding({ "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Column.height({ "id": 134217920, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Column.width(StyleConstants.FULL_WIDTH);
            Column.backgroundColor(Color.White);
            Column.borderRadius({ "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create($rawfile(info.images[0]));
            Image.debugLine("../../../../../common/src/main/ets/components/CommodityList.ets(32:7)");
            Image.width({ "id": 134217921, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.height({ "id": 134217921, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.objectFit(ImageFit.Contain);
            Image.margin({
                top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../common/src/main/ets/components/CommodityList.ets(40:7)");
            Column.width(StyleConstants.FULL_WIDTH);
            Column.alignItems(HorizontalAlign.Start);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217893, "type": 10003, params: [info.title, info.description], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../common/src/main/ets/components/CommodityList.ets(41:9)");
            Text.fontColor(Color.Black);
            Text.maxLines(StyleConstants.TWO_TEXT_LINE);
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(StyleConstants.FONT_WEIGHT_FOUR);
            Text.lineHeight({ "id": 134217919, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217892, "type": 10003, params: [info.price], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../common/src/main/ets/components/CommodityList.ets(48:9)");
            Text.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.margin({
                top: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`${info.promotion}`);
            Text.debugLine("../../../../../common/src/main/ets/components/CommodityList.ets(55:9)");
            Text.fontSize({ "id": 134217927, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor(Color.White);
            Text.backgroundColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.borderRadius({ "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.height({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.padding({
                right: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        Column.pop();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.commodityList.length > 0) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        List.create({ space: StyleConstants.TWELVE_SPACE });
                        List.debugLine("../../../../../common/src/main/ets/components/CommodityList.ets(78:7)");
                        List.margin({ left: { "id": 134217922, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, right: { "id": 134217922, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                        List.listDirection(Axis.Vertical);
                        List.lanes(this.column);
                        if (!isInitialRender) {
                            List.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    {
                        const __lazyForEachItemGenFunction = _item => {
                            const item = _item;
                            {
                                const isLazyCreate = (globalThis.__lazyForEachItemGenFunction !== undefined) && true;
                                const itemCreation = (elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    ListItem.create(deepRenderFunction, isLazyCreate);
                                    ListItem.margin({ left: { "id": 134217937, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, right: { "id": 134217937, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                                    ListItem.onClick(() => this.onClickItem(item));
                                    ListItem.debugLine("../../../../../common/src/main/ets/components/CommodityList.ets(80:11)");
                                    if (!isInitialRender) {
                                        ListItem.pop();
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                };
                                const observedShallowRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    ListItem.pop();
                                };
                                const observedDeepRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    this.CommodityItem.bind(this)(item);
                                    ListItem.pop();
                                };
                                const deepRenderFunction = (elmtId, isInitialRender) => {
                                    itemCreation(elmtId, isInitialRender);
                                    this.updateFuncByElmtId.set(elmtId, itemCreation);
                                    this.CommodityItem.bind(this)(item);
                                    ListItem.pop();
                                };
                                if (isLazyCreate) {
                                    observedShallowRender();
                                }
                                else {
                                    observedDeepRender();
                                }
                            }
                        };
                        const __lazyForEachItemIdFunc = item => JSON.stringify(item);
                        LazyForEach.create("1", this, new CommonDataSource(this.commodityList), __lazyForEachItemGenFunction, __lazyForEachItemIdFunc);
                        LazyForEach.pop();
                    }
                    List.pop();
                });
            }
            else {
                this.ifElseBranchUpdateFunction(1, () => {
                    {
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            if (isInitialRender) {
                                ViewPU.create(new EmptyComponent(this, { outerHeight: StyleConstants.FIFTY_HEIGHT }, undefined, elmtId));
                            }
                            else {
                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                    }
                });
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=CommodityList.js.map