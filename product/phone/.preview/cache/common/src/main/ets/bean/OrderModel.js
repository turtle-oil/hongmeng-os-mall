var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export var OrderType;
(function (OrderType) {
    OrderType["PAYMENT"] = "payment";
    OrderType["SHIP"] = "ship";
    OrderType["RECEIPT"] = "receipt";
    OrderType["EVALUATION"] = "evaluation";
    OrderType["SALE"] = "sale";
})(OrderType || (OrderType = {}));
export var OrderOperationStatus;
(function (OrderOperationStatus) {
    OrderOperationStatus[OrderOperationStatus["UN_PAY"] = 0] = "UN_PAY";
    OrderOperationStatus[OrderOperationStatus["DELIVERED"] = 1] = "DELIVERED";
    OrderOperationStatus[OrderOperationStatus["RECEIPT"] = 2] = "RECEIPT";
    OrderOperationStatus[OrderOperationStatus["CONSIGNMENT"] = 3] = "CONSIGNMENT";
    OrderOperationStatus[OrderOperationStatus["ALLStatus"] = 4] = "ALLStatus";
})(OrderOperationStatus || (OrderOperationStatus = {}));
let Order = class Order {
    constructor(props) {
        this.uid = '';
        this.orderId = '';
        this.image = '';
        this.title = '';
        this.description = '';
        this.payTime = '';
        this.orderTime = '';
        this.createTime = '';
        this.updateTime = '';
        this.price = 0;
        this.count = 0;
        this.amount = 0;
        this.status = 0;
        this.commodityId = 0;
        this.specifications = {};
        if (!props) {
            return;
        }
        this.uid = props.uid;
        this.orderId = props.orderId;
        this.image = props.image;
        this.title = props.title;
        this.description = props.description;
        this.payTime = props.payTime;
        this.orderTime = props.orderTime;
        this.createTime = props.createTime;
        this.updateTime = props.updateTime;
        this.price = props.price;
        this.count = props.count;
        this.amount = props.amount;
        this.status = props.status;
        this.commodityId = props.commodityId;
        this.specifications = props.specifications;
    }
};
Order = __decorate([
    Observed
], Order);
export { Order };
//# sourceMappingURL=OrderModel.js.map