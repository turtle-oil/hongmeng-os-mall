/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OrderType } from '@bundle:com.example.multishopping/phone@common/index';
import { IconButtonModel } from '@bundle:com.example.multishopping/phone@personal/ets/bean/IconButtonModel';
const orderButton = [
    new IconButtonModel({ "id": 134217981, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, { "id": 134217779, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderType.PAYMENT, 0),
    new IconButtonModel({ "id": 134217954, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, { "id": 134217977, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderType.SHIP, 0),
    new IconButtonModel({ "id": 134217992, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, { "id": 134217976, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderType.RECEIPT, 0),
    new IconButtonModel({ "id": 134217988, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, { "id": 134217975, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderType.EVALUATION, 0),
    new IconButtonModel({ "id": 134217989, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, { "id": 134217974, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderType.SALE, 0)
];
const memberButton = [
    {
        icon: { "id": 134217987, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217968, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    },
    {
        icon: { "id": 134217991, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217961, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    },
    {
        icon: { "id": 134217990, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217964, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    },
    {
        icon: { "id": 134217955, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217962, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    }
];
const activityButton = [
    {
        icon: { "id": 134217985, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217971, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    },
    {
        icon: { "id": 134217986, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217963, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    },
    {
        icon: { "id": 134217984, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217979, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    },
    {
        icon: { "id": 134217982, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217969, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    }
];
const liveData = [
    {
        'liveId': '1',
        'living': true,
        'watchDesc': '2.9万人观看',
        'previewIcon': 'common/food.png',
        'liverIcon': 'common/ic_person.png',
        'liverName': '首席美食家',
        'liverDesc': '带您品尝各地美食'
    },
    {
        'liveId': '2',
        'living': true,
        'watchDesc': '2.9万人观看',
        'previewIcon': 'common/scenery.png',
        'liverIcon': 'common/ic_person.png',
        'liverName': '大旅游家',
        'liverDesc': '带您走遍天下奇景'
    },
    {
        'liveId': '3',
        'living': false,
        'watchDesc': '0人观看',
        'previewIcon': 'common/food.png',
        'liverIcon': 'common/ic_person.png',
        'liverName': '首席美食家',
        'liverDesc': '带您品尝各地美食'
    },
    {
        'liveId': '4',
        'living': true,
        'watchDesc': '2.9万人观看',
        'previewIcon': 'common/scenery.png',
        'liverIcon': 'common/ic_person.png',
        'liverName': '大旅游家',
        'liverDesc': '带您走遍天下奇景'
    },
    {
        'liveId': '5',
        'living': true,
        'watchDesc': '2.9万人观看',
        'previewIcon': 'common/food.png',
        'liverIcon': 'common/ic_person.png',
        'liverName': '首席美食家',
        'liverDesc': '带您品尝各地美食'
    },
    {
        'liveId': '6',
        'living': true,
        'watchDesc': '2.9万人观看',
        'previewIcon': 'common/scenery.png',
        'liverIcon': 'common/ic_person.png',
        'liverName': '大旅游家',
        'liverDesc': '带您走遍天下奇景'
    },
    {
        'liveId': '7',
        'living': true,
        'watchDesc': '2.9万人观看',
        'previewIcon': 'common/food.png',
        'liverIcon': 'common/ic_person.png',
        'liverName': '首席美食家',
        'liverDesc': '带您品尝各地美食'
    },
    {
        'liveId': '8',
        'living': true,
        'watchDesc': '2.9万人观看',
        'previewIcon': 'common/scenery.png',
        'liverIcon': 'common/ic_person.png',
        'liverName': '大旅游家',
        'liverDesc': '带您走遍天下奇景'
    }
];
export { orderButton, memberButton, activityButton, liveData };
//# sourceMappingURL=PersonalData.js.map