import { PersonalConstants } from '@bundle:com.example.multishopping/phone@personal/ets/constants/PersonalConstants';
export class IconButton extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__props = new SynchedPropertyNesedObjectPU(params.props, this, "props");
        this.click = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        this.__props.set(params.props);
        if (params.click !== undefined) {
            this.click = params.click;
        }
    }
    updateStateVars(params) {
        this.__props.set(params.props);
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__props.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__props.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get props() {
        return this.__props.get();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            var _a;
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/personal/src/main/ets/components/IconButton.ets(25:5)");
            Column.height({ "id": 134217933, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Column.onClick((_a = this.click) === null || _a === void 0 ? void 0 : _a.bind(this.props.key));
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.props.count) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Badge.create({
                            value: `${this.props.count}`,
                            style: { color: Color.White, badgeSize: PersonalConstants.BADGE_SIZE, badgeColor: Color.Red }
                        });
                        Badge.debugLine("../../../../../features/personal/src/main/ets/components/IconButton.ets(27:9)");
                        Badge.width({ "id": 134217949, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Badge.height({ "id": 134217949, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        if (!isInitialRender) {
                            Badge.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Image.create(this.props.icon);
                        Image.debugLine("../../../../../features/personal/src/main/ets/components/IconButton.ets(31:11)");
                        Image.width({ "id": 134217949, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Image.height({ "id": 134217949, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        if (!isInitialRender) {
                            Image.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Badge.pop();
                });
            }
            else {
                this.ifElseBranchUpdateFunction(1, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Image.create(this.props.icon);
                        Image.debugLine("../../../../../features/personal/src/main/ets/components/IconButton.ets(38:9)");
                        Image.width({ "id": 134217949, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Image.height({ "id": 134217949, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        if (!isInitialRender) {
                            Image.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                });
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(this.props.text);
            Text.debugLine("../../../../../features/personal/src/main/ets/components/IconButton.ets(42:7)");
            Text.fontSize({ "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor(Color.Black);
            Text.margin({ top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=IconButton.js.map