/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class PersonalConstants {
}
/**
 * Index of pending payment tab.
 */
PersonalConstants.PENDING_PAYMENT_INDEX = 1;
/**
 * Index of waiting shipping tab.
 */
PersonalConstants.WAITING_SHIPMENT_INDEX = 2;
/**
 * Index of waiting received tab.
 */
PersonalConstants.WAITING_RECEIVED_INDEX = 3;
/**
 * Index of waiting evaluate tab.
 */
PersonalConstants.WAITING_EVALUATE_INDEX = 4;
/**
* Size of badge.
*/
PersonalConstants.BADGE_SIZE = 16;
/**
 * Live list item image aspect ration of sm.
 */
PersonalConstants.ASPECT_RATIO_SM = 1.09;
/**
 * Live list item image aspect ration of md.
 */
PersonalConstants.ASPECT_RATIO_MD = 1.51;
/**
 * Live list item image aspect ration of lg.
 */
PersonalConstants.ASPECT_RATIO_LG = 1.63;
/**
 * OrderDetailListPage url.
 */
PersonalConstants.ORDER_LIST_PAGE_URL = 'pages/OrderDetailListPage';
//# sourceMappingURL=PersonalConstants.js.map