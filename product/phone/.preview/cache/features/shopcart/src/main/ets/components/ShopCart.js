/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@ohos:router';
import { ShopCartConstants } from '@bundle:com.example.multishopping/phone@shopcart/ets/constants/ShopCartConstants';
import { LocalDataManager, Logger, CounterProduct, CommodityList, EmptyComponent, StyleConstants, BreakpointConstants } from '@bundle:com.example.multishopping/phone@common/index';
export class ShopCart extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.onNeedUpdate = undefined;
        this.__currentBreakpoint = this.createStorageProp('currentBreakpoint', 'sm', "currentBreakpoint");
        this.__products = new SynchedPropertyObjectTwoWayPU(params.products, this, "products");
        this.__sumPrice = new ObservedPropertySimplePU(0, this, "sumPrice");
        this.__isSelectAll = new ObservedPropertySimplePU(false, this, "isSelectAll");
        this.__commodityList = new ObservedPropertyObjectPU([], this, "commodityList");
        this.__selectProducts = new ObservedPropertyObjectPU({}, this, "selectProducts");
        this.localDataManager = LocalDataManager.instance();
        this.onChangeCount = (count, info) => {
            this.localDataManager.updateShopCart({
                id: info.id,
                count: count
            });
            this.needUpdateShopCart();
        };
        this.setInitiallyProvidedValue(params);
        this.declareWatch("products", this.onListChange);
    }
    setInitiallyProvidedValue(params) {
        if (params.onNeedUpdate !== undefined) {
            this.onNeedUpdate = params.onNeedUpdate;
        }
        if (params.sumPrice !== undefined) {
            this.sumPrice = params.sumPrice;
        }
        if (params.isSelectAll !== undefined) {
            this.isSelectAll = params.isSelectAll;
        }
        if (params.commodityList !== undefined) {
            this.commodityList = params.commodityList;
        }
        if (params.selectProducts !== undefined) {
            this.selectProducts = params.selectProducts;
        }
        if (params.localDataManager !== undefined) {
            this.localDataManager = params.localDataManager;
        }
        if (params.onChangeCount !== undefined) {
            this.onChangeCount = params.onChangeCount;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__products.purgeDependencyOnElmtId(rmElmtId);
        this.__sumPrice.purgeDependencyOnElmtId(rmElmtId);
        this.__isSelectAll.purgeDependencyOnElmtId(rmElmtId);
        this.__commodityList.purgeDependencyOnElmtId(rmElmtId);
        this.__selectProducts.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__currentBreakpoint.aboutToBeDeleted();
        this.__products.aboutToBeDeleted();
        this.__sumPrice.aboutToBeDeleted();
        this.__isSelectAll.aboutToBeDeleted();
        this.__commodityList.aboutToBeDeleted();
        this.__selectProducts.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get currentBreakpoint() {
        return this.__currentBreakpoint.get();
    }
    set currentBreakpoint(newValue) {
        this.__currentBreakpoint.set(newValue);
    }
    get products() {
        return this.__products.get();
    }
    set products(newValue) {
        this.__products.set(newValue);
    }
    get sumPrice() {
        return this.__sumPrice.get();
    }
    set sumPrice(newValue) {
        this.__sumPrice.set(newValue);
    }
    get isSelectAll() {
        return this.__isSelectAll.get();
    }
    set isSelectAll(newValue) {
        this.__isSelectAll.set(newValue);
    }
    get commodityList() {
        return this.__commodityList.get();
    }
    set commodityList(newValue) {
        this.__commodityList.set(newValue);
    }
    get selectProducts() {
        return this.__selectProducts.get();
    }
    set selectProducts(newValue) {
        this.__selectProducts.set(newValue);
    }
    aboutToAppear() {
        const sortRes = this.localDataManager.queryCommodityList();
        sortRes.sort(() => (Math.random() - StyleConstants.HALF_ONE) > 0 ? 1 : StyleConstants.MINUS_ONE);
        this.commodityList = [...sortRes];
        this.onListChange();
    }
    needUpdateShopCart() {
        this.onNeedUpdate();
        this.countSumPrice();
    }
    countSumPrice() {
        this.sumPrice = 0;
        this.isSelectAll = Object.values(this.selectProducts).every(item => item);
        this.sumPrice = Object.keys(this.selectProducts).reduce((sum, key) => {
            const selected = this.selectProducts[key];
            const data = this.products.find(item => item.id === key);
            const ins = (selected ? data.price * data.count : 0);
            return sum + ins;
        }, 0);
    }
    settleAccounts() {
        const orderList = this.products
            .filter(item => this.selectProducts[item.id])
            .map(item => {
            return {
                id: item.id,
                commodityId: item.commodityId,
                price: item.price,
                count: item.count,
                specifications: item.specifications,
                images: [item.img],
                description: item.description,
                title: item.name
            };
        });
        Logger.info('settleAccounts orderList length: ' + orderList.length);
        router.pushUrl({
            url: ShopCartConstants.CONFIRM_ORDER_PAGE_URL,
            params: { orderList: orderList }
        }).catch(err => {
            Logger.error(JSON.stringify(err));
        });
    }
    onListChange() {
        let payload = {};
        this.products.forEach(item => {
            payload[item.id] = !!item.selected;
        });
        this.selectProducts = payload;
        this.countSumPrice();
    }
    ItemDelete(item, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({
                direction: FlexDirection.Column,
                justifyContent: FlexAlign.Center,
                alignItems: ItemAlign.End
            });
            Flex.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(106:5)");
            Flex.onClick(() => {
                this.products = this.localDataManager.deleteShopCart([item.id]);
            });
            Flex.height({ "id": 134217996, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.width({ "id": 134217998, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.backgroundColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.margin({ left: { "id": 134217997, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(111:7)");
            Column.padding({ right: { "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217993, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(112:9)");
            Image.width({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.height({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.margin({ bottom: { "id": 134217939, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134218002, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(116:9)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor(Color.White);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        Flex.pop();
    }
    CartItem(item, index, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Row, alignItems: ItemAlign.Center });
            Flex.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(133:5)");
            Flex.padding({
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Flex.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.backgroundColor(Color.White);
            Flex.width(StyleConstants.FULL_WIDTH);
            Flex.height({ "id": 134217996, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Checkbox.create({
                name: `${ShopCartConstants.CHECKBOX}${index}`,
                group: ShopCartConstants.CHECKBOX_GROUP
            });
            Checkbox.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(134:7)");
            Checkbox.width({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Checkbox.height({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Checkbox.selectedColor({ "id": 134217994, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Checkbox.select(!!this.selectProducts[item.id]);
            Checkbox.onClick(() => {
                this.selectProducts[item.id] = !this.selectProducts[item.id];
                this.localDataManager.updateShopCart({
                    id: item.id,
                    selected: this.selectProducts[item.id]
                });
                this.needUpdateShopCart();
            });
            if (!isInitialRender) {
                Checkbox.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Checkbox.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create($rawfile(item.img));
            Image.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(150:7)");
            Image.height({ "id": 134217995, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.width({ "id": 134217995, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.objectFit(ImageFit.Cover);
            Image.margin({ left: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column, justifyContent: FlexAlign.SpaceAround });
            Flex.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(155:7)");
            Flex.margin({
                left: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Flex.width(StyleConstants.FULL_WIDTH);
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217893, "type": 10003, params: [item.name, item.description], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(156:9)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.margin({ bottom: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.maxLines(StyleConstants.TWO_TEXT_LINE);
            Text.width(StyleConstants.FULL_WIDTH);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`${Object.values(item.specifications).join('，')}`);
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(162:9)");
            Text.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.maxLines(1);
            Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.width(StyleConstants.FULL_WIDTH);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ justifyContent: FlexAlign.SpaceBetween });
            Flex.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(168:9)");
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create();
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(169:11)");
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create({ "id": 134217841, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(170:13)");
            Span.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create(`${item.price}`);
            Span.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(173:13)");
            Span.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CounterProduct(this, {
                        count: item.count,
                        onNumberChange: (num) => {
                            this.onChangeCount(num, item);
                        }
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Flex.pop();
        Flex.pop();
        Flex.pop();
    }
    Settle(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ justifyContent: FlexAlign.SpaceBetween });
            Flex.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(204:5)");
            Flex.padding({
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Flex.backgroundColor(Color.White);
            Flex.width(StyleConstants.FULL_WIDTH);
            Flex.height({ "id": 134217933, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ alignItems: ItemAlign.Center });
            Flex.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(205:7)");
            Flex.height(StyleConstants.FULL_HEIGHT);
            Flex.width({ "id": 134218000, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Checkbox.create({ name: ShopCartConstants.CHECKBOX, group: ShopCartConstants.CHECKBOX_GROUP });
            Checkbox.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(206:9)");
            Checkbox.selectedColor({ "id": 134217994, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Checkbox.select(this.isSelectAll);
            Checkbox.onClick(() => {
                this.isSelectAll = !this.isSelectAll;
                this.products.map(item => {
                    return this.localDataManager.updateShopCart({
                        id: item.id,
                        count: item.count,
                        selected: !!this.isSelectAll,
                        specifications: {}
                    });
                });
                this.needUpdateShopCart();
            });
            if (!isInitialRender) {
                Checkbox.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Checkbox.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134218004, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(221:9)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor(Color.Grey);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Flex.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.End });
            Flex.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(228:7)");
            Flex.height(StyleConstants.FULL_HEIGHT);
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134218005, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(229:9)");
            Text.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(FontWeight.Bold);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create();
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(232:9)");
            Text.margin({
                right: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create({ "id": 134217841, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(233:11)");
            Span.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create(`${this.sumPrice}`);
            Span.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(236:11)");
            Span.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel({ "id": 134218001, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, { type: ButtonType.Capsule, stateEffect: true });
            Button.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(245:9)");
            Button.backgroundColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Button.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Button.height({ "id": 134217999, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Button.width({ "id": 134218000, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Button.onClick(() => {
                this.settleAccounts();
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        Flex.pop();
        Flex.pop();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column });
            Flex.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(266:5)");
            Flex.width(StyleConstants.FULL_WIDTH);
            Flex.backgroundColor({ "id": 134217905, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217732, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(267:7)");
            Text.fontSize({ "id": 134217926, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.height({ "id": 134217933, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.padding({ left: { "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            Text.width(StyleConstants.FULL_WIDTH);
            Text.textAlign(TextAlign.Start);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Scroll.create();
            Scroll.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(273:7)");
            Scroll.scrollBar(BarState.Off);
            Scroll.margin({
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Scroll.height(StyleConstants.FULL_HEIGHT);
            if (!isInitialRender) {
                Scroll.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(274:9)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.products.length > 0) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        List.create({ space: StyleConstants.FIFTEEN_SPACE });
                        List.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(276:13)");
                        if (!isInitialRender) {
                            List.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        ForEach.create();
                        const forEachItemGenFunction = (_item, index) => {
                            const item = _item;
                            {
                                const isLazyCreate = true;
                                const itemCreation = (elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    ListItem.create(deepRenderFunction, isLazyCreate);
                                    ListItem.swipeAction({ end: this.ItemDelete.bind(this, item) });
                                    ListItem.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(278:17)");
                                    if (!isInitialRender) {
                                        ListItem.pop();
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                };
                                const observedShallowRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    ListItem.pop();
                                };
                                const observedDeepRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    this.CartItem.bind(this)(item, index);
                                    ListItem.pop();
                                };
                                const deepRenderFunction = (elmtId, isInitialRender) => {
                                    itemCreation(elmtId, isInitialRender);
                                    this.updateFuncByElmtId.set(elmtId, itemCreation);
                                    this.CartItem.bind(this)(item, index);
                                    ListItem.pop();
                                };
                                if (isLazyCreate) {
                                    observedShallowRender();
                                }
                                else {
                                    observedDeepRender();
                                }
                            }
                        };
                        this.forEachUpdateFunction(elmtId, this.products, forEachItemGenFunction, item => item.id, true, false);
                        if (!isInitialRender) {
                            ForEach.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    ForEach.pop();
                    List.pop();
                });
            }
            else {
                this.ifElseBranchUpdateFunction(1, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Column.create();
                        Column.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(285:13)");
                        Column.width(StyleConstants.FULL_WIDTH);
                        Column.height(StyleConstants.FIFTY_HEIGHT);
                        if (!isInitialRender) {
                            Column.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    {
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            if (isInitialRender) {
                                ViewPU.create(new EmptyComponent(this, {}, undefined, elmtId));
                            }
                            else {
                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                    }
                    Column.pop();
                });
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134218003, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/shopcart/src/main/ets/components/ShopCart.ets(291:11)");
            Text.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.width(StyleConstants.FULL_WIDTH);
            Text.margin({
                top: { "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Text.textAlign(TextAlign.Start);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CommodityList(this, {
                        commodityList: this.__commodityList,
                        column: this.currentBreakpoint === BreakpointConstants.BREAKPOINT_SM ?
                            StyleConstants.DISPLAY_TWO : StyleConstants.DISPLAY_THREE,
                        onClickItem: (data) => {
                            router.pushUrl({
                                url: ShopCartConstants.COMMODITY_DETAIL_PAGE_URL,
                                params: { id: data.id }
                            }).catch(err => {
                                Logger.error(JSON.stringify(err));
                            });
                        }
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {
                        column: this.currentBreakpoint === BreakpointConstants.BREAKPOINT_SM ?
                            StyleConstants.DISPLAY_TWO : StyleConstants.DISPLAY_THREE
                    });
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
        Scroll.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if ((Object.values(ObservedObject.GetRawObject(this.selectProducts))).some((i => !!i))) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.Settle.bind(this)();
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        Flex.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=ShopCart.js.map