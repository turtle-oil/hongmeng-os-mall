/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import promptAction from '@ohos:promptAction';
import router from '@ohos:router';
import { AddressInfo } from '@bundle:com.example.multishopping/phone@orderdetail/ets/components/AddressInfo';
import { HeaderBar } from '@bundle:com.example.multishopping/phone@orderdetail/ets/components/HeaderBar';
import { OrderDetailConstants } from '@bundle:com.example.multishopping/phone@orderdetail/ets/constants/OrderDetailConstants';
import { CommodityOrderList } from '@bundle:com.example.multishopping/phone@orderdetail/ets/components/CommodityOrderList';
import { LocalDataManager, OrderOperationStatus, GridConstants, StyleConstants } from '@bundle:com.example.multishopping/phone@common/index';
export class ConfirmOrder extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__orderList = this.initializeConsume("orderList", "orderList");
        this.__amount = this.initializeConsume("amount", "amount");
        this.orderIds = [];
        this.localDataManager = LocalDataManager.instance();
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.orderIds !== undefined) {
            this.orderIds = params.orderIds;
        }
        if (params.localDataManager !== undefined) {
            this.localDataManager = params.localDataManager;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__orderList.aboutToBeDeleted();
        this.__amount.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get orderList() {
        return this.__orderList.get();
    }
    set orderList(newValue) {
        this.__orderList.set(newValue);
    }
    get amount() {
        return this.__amount.get();
    }
    set amount(newValue) {
        this.__amount.set(newValue);
    }
    /**
     * Confirm order to request server.
     */
    confirmOrder(status) {
        this.orderList.map((item) => {
            const orderId = this.localDataManager.insertOrder({ order: item, status: status });
            this.localDataManager.deleteShopCart([orderId]);
            this.orderIds.push(orderId);
        });
    }
    handleConfirmOrder() {
        try {
            this.confirmOrder(OrderOperationStatus.UN_PAY);
            promptAction.showToast({ message: { "id": 134217771, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            router.replaceUrl({
                url: OrderDetailConstants.PAY_ORDER_PAGE_URL,
                params: { orderIds: this.orderIds }
            });
        }
        catch (err) {
            promptAction.showToast({ message: { "id": 134217770, "type": 10003, params: [err.message], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
        }
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column });
            Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(56:5)");
            Flex.backgroundColor({ "id": 134217905, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.height(StyleConstants.FULL_HEIGHT);
            Flex.width(StyleConstants.FULL_WIDTH);
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new HeaderBar(this, {
                        title: { "id": 134217766, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                        onBack: () => router.back()
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(61:7)");
            Column.flexGrow(StyleConstants.FLEX_GROW);
            Column.padding({ left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Scroll.create();
            Scroll.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(62:9)");
            Scroll.scrollBar(BarState.Off);
            if (!isInitialRender) {
                Scroll.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridRow.create({
                columns: {
                    sm: GridConstants.COLUMN_FOUR,
                    md: GridConstants.COLUMN_EIGHT,
                    lg: GridConstants.COLUMN_TWELVE
                }
            });
            GridRow.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(63:11)");
            if (!isInitialRender) {
                GridRow.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridCol.create({
                span: {
                    sm: GridConstants.SPAN_FOUR,
                    md: GridConstants.SPAN_EIGHT,
                    lg: GridConstants.SPAN_EIGHT
                },
                offset: { lg: GridConstants.OFFSET_TWO }
            });
            GridCol.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(70:13)");
            if (!isInitialRender) {
                GridCol.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(78:15)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new AddressInfo(this, {}, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CommodityOrderList(this, {}, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
        GridCol.pop();
        GridRow.pop();
        Scroll.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridRow.create({
                columns: {
                    sm: GridConstants.COLUMN_FOUR,
                    md: GridConstants.COLUMN_EIGHT,
                    lg: GridConstants.COLUMN_TWELVE
                },
                gutter: GridConstants.GUTTER_TWELVE
            });
            GridRow.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(90:7)");
            GridRow.padding({
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            GridRow.border({
                color: { "id": 134217902, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                width: { top: { "id": 134217936, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } }
            });
            if (!isInitialRender) {
                GridRow.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridCol.create({
                span: {
                    sm: GridConstants.SPAN_TWO,
                    md: GridConstants.SPAN_TWO,
                    lg: GridConstants.SPAN_TWO
                },
                offset: { lg: GridConstants.OFFSET_TWO }
            });
            GridCol.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(98:9)");
            if (!isInitialRender) {
                GridCol.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217758, "type": 10003, params: [this.amount], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(106:11)");
            Text.fontSize({ "id": 134217926, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.margin({ right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            Text.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.textAlign(TextAlign.Start);
            Text.width(StyleConstants.FULL_WIDTH);
            Text.height({ "id": 134217933, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        GridCol.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridCol.create({
                span: {
                    sm: GridConstants.SPAN_TWO,
                    md: GridConstants.SPAN_THREE,
                    lg: GridConstants.SPAN_THREE
                },
                offset: {
                    md: GridConstants.OFFSET_THREE,
                    lg: GridConstants.OFFSET_THREE
                }
            });
            GridCol.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(115:9)");
            if (!isInitialRender) {
                GridCol.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel({ "id": 134217759, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Button.debugLine("../../../../../features/orderdetail/src/main/ets/components/ConfirmOrder.ets(126:11)");
            Button.backgroundColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Button.height({ "id": 134217788, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Button.width(StyleConstants.FULL_WIDTH);
            Button.margin({ top: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            Button.onClick(() => this.handleConfirmOrder());
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        GridCol.pop();
        GridRow.pop();
        Flex.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=ConfirmOrder.js.map