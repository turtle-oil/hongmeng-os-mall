/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import promptAction from '@ohos:promptAction';
import router from '@ohos:router';
import { CommodityOrderItem } from '@bundle:com.example.multishopping/phone@orderdetail/ets/components/CommodityOrderItem';
import { payEnum, statusEnum } from '@bundle:com.example.multishopping/phone@orderdetail/ets/viewmodel/OrderData';
import { OrderDetailConstants } from '@bundle:com.example.multishopping/phone@orderdetail/ets/constants/OrderDetailConstants';
import { LocalDataManager, Logger, OrderOperationStatus, EmptyComponent, CommodityList, StyleConstants, BreakpointConstants, GridConstants } from '@bundle:com.example.multishopping/phone@common/index';
export class OrderListContent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__currentBreakpoint = this.createStorageProp('currentBreakpoint', 'sm', "currentBreakpoint");
        this.__orderList = this.initializeConsume("orderList", "orderList");
        this.__commodityList = this.initializeConsume("commodityList", "commodityList");
        this.status = OrderOperationStatus.ALLStatus;
        this.localDataManager = LocalDataManager.instance();
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.status !== undefined) {
            this.status = params.status;
        }
        if (params.localDataManager !== undefined) {
            this.localDataManager = params.localDataManager;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__currentBreakpoint.aboutToBeDeleted();
        this.__orderList.aboutToBeDeleted();
        this.__commodityList.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get currentBreakpoint() {
        return this.__currentBreakpoint.get();
    }
    set currentBreakpoint(newValue) {
        this.__currentBreakpoint.set(newValue);
    }
    get orderList() {
        return this.__orderList.get();
    }
    set orderList(newValue) {
        this.__orderList.set(newValue);
    }
    get commodityList() {
        return this.__commodityList.get();
    }
    set commodityList(newValue) {
        this.__commodityList.set(newValue);
    }
    paymentHandler(orderId, status, msg) {
        this.orderList = this.localDataManager.updateOrder({ orderId, status });
        promptAction.showToast({ message: `${msg}${OrderDetailConstants.SUCCESS}` });
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(48:5)");
            Column.margin({
                top: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Scroll.create();
            Scroll.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(49:7)");
            Scroll.scrollBar(BarState.Off);
            if (!isInitialRender) {
                Scroll.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridRow.create({
                columns: {
                    sm: GridConstants.COLUMN_FOUR,
                    md: GridConstants.COLUMN_EIGHT,
                    lg: GridConstants.COLUMN_TWELVE
                }
            });
            GridRow.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(50:9)");
            if (!isInitialRender) {
                GridRow.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridCol.create({
                span: {
                    sm: GridConstants.SPAN_FOUR,
                    md: GridConstants.SPAN_EIGHT,
                    lg: GridConstants.SPAN_EIGHT
                },
                offset: { lg: GridConstants.OFFSET_TWO }
            });
            GridCol.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(57:11)");
            if (!isInitialRender) {
                GridCol.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(65:13)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.status === OrderOperationStatus.ALLStatus ||
                this.orderList.filter(item => item.status === this.status).length > 0) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        List.create();
                        List.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(68:17)");
                        if (!isInitialRender) {
                            List.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        ForEach.create();
                        const forEachItemGenFunction = _item => {
                            const info = _item;
                            {
                                const isLazyCreate = true;
                                const itemCreation = (elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    ListItem.create(deepRenderFunction, isLazyCreate);
                                    ListItem.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(71:21)");
                                    if (!isInitialRender) {
                                        ListItem.pop();
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                };
                                const observedShallowRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    ListItem.pop();
                                };
                                const observedDeepRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Flex.create({ direction: FlexDirection.Column });
                                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(72:23)");
                                        Flex.onClick(() => {
                                            router.pushUrl({
                                                url: OrderDetailConstants.PAY_ORDER_PAGE_URL,
                                                params: {
                                                    order: info
                                                }
                                            }).catch(err => {
                                                Logger.error(JSON.stringify(err));
                                            });
                                        });
                                        Flex.width(StyleConstants.FULL_WIDTH);
                                        Flex.backgroundColor(Color.White);
                                        Flex.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Flex.margin({
                                            bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        Flex.padding({
                                            left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                            right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                            top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                            bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Flex.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Flex.create({ justifyContent: FlexAlign.End });
                                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(73:25)");
                                        Flex.height({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Flex.margin({
                                            top: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                            bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        Flex.width(StyleConstants.FULL_WIDTH);
                                        if (!isInitialRender) {
                                            Flex.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Text.create(statusEnum[info.status]);
                                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(74:27)");
                                        Text.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        if (!isInitialRender) {
                                            Text.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    Text.pop();
                                    Flex.pop();
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Divider.create();
                                        Divider.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(85:25)");
                                        Divider.width(StyleConstants.FULL_WIDTH);
                                        Divider.height({ "id": 134217936, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Divider.backgroundColor({ "id": 134217902, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Divider.margin({
                                            bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Divider.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    {
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            if (isInitialRender) {
                                                ViewPU.create(new CommodityOrderItem(this, { orderData: info }, undefined, elmtId));
                                            }
                                            else {
                                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                    }
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Text.create({ "id": 134217772, "type": 10003, params: [info.orderId], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(94:25)");
                                        Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.width(StyleConstants.FULL_WIDTH);
                                        Text.margin({
                                            top: { "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Text.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    Text.pop();
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Divider.create();
                                        Divider.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(101:25)");
                                        Divider.width(StyleConstants.FULL_WIDTH);
                                        Divider.height({ "id": 134217936, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Divider.backgroundColor({ "id": 134217902, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Divider.margin({
                                            top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Divider.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Flex.create({
                                            direction: FlexDirection.Column,
                                            alignItems: ItemAlign.End,
                                            justifyContent: FlexAlign.End
                                        });
                                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(108:25)");
                                        Flex.height({ "id": 134217792, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Flex.padding({
                                            top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Flex.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Text.create({ "id": 134217764, "type": 10003, params: [info.count,
                                                payEnum[info.status], info.price * info.count], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(113:27)");
                                        Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.margin({
                                            bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Text.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    Text.pop();
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Flex.create({ justifyContent: FlexAlign.End });
                                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(119:27)");
                                        Flex.width(StyleConstants.FULL_WIDTH);
                                        if (!isInitialRender) {
                                            Flex.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        If.create();
                                        if (info.status === OrderOperationStatus.UN_PAY) {
                                            this.ifElseBranchUpdateFunction(0, () => {
                                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                                    Button.createWithLabel({ "id": 134217778, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(121:31)");
                                                    Button.height({ "id": 134217791, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.fontColor(Color.White);
                                                    Button.backgroundColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.borderRadius({ "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.onClick(() => {
                                                        this.paymentHandler(info.orderId, OrderOperationStatus.DELIVERED, OrderDetailConstants.ORDER_PAY);
                                                    });
                                                    if (!isInitialRender) {
                                                        Button.pop();
                                                    }
                                                    ViewStackProcessor.StopGetAccessRecording();
                                                });
                                                Button.pop();
                                            });
                                        }
                                        else if (info.status === OrderOperationStatus.DELIVERED) {
                                            this.ifElseBranchUpdateFunction(1, () => {
                                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                                    Button.createWithLabel({ "id": 134217761, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(131:31)");
                                                    Button.height({ "id": 134217791, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.fontColor(Color.White);
                                                    Button.backgroundColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.borderRadius({ "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.onClick(() => {
                                                        this.paymentHandler(info.orderId, OrderOperationStatus.RECEIPT, OrderDetailConstants.ORDER_CONFIRM);
                                                    });
                                                    if (!isInitialRender) {
                                                        Button.pop();
                                                    }
                                                    ViewStackProcessor.StopGetAccessRecording();
                                                });
                                                Button.pop();
                                            });
                                        }
                                        if (!isInitialRender) {
                                            If.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    If.pop();
                                    Flex.pop();
                                    Flex.pop();
                                    Flex.pop();
                                    ListItem.pop();
                                };
                                const deepRenderFunction = (elmtId, isInitialRender) => {
                                    itemCreation(elmtId, isInitialRender);
                                    this.updateFuncByElmtId.set(elmtId, itemCreation);
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Flex.create({ direction: FlexDirection.Column });
                                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(72:23)");
                                        Flex.onClick(() => {
                                            router.pushUrl({
                                                url: OrderDetailConstants.PAY_ORDER_PAGE_URL,
                                                params: {
                                                    order: info
                                                }
                                            }).catch(err => {
                                                Logger.error(JSON.stringify(err));
                                            });
                                        });
                                        Flex.width(StyleConstants.FULL_WIDTH);
                                        Flex.backgroundColor(Color.White);
                                        Flex.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Flex.margin({
                                            bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        Flex.padding({
                                            left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                            right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                            top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                            bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Flex.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Flex.create({ justifyContent: FlexAlign.End });
                                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(73:25)");
                                        Flex.height({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Flex.margin({
                                            top: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                            bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        Flex.width(StyleConstants.FULL_WIDTH);
                                        if (!isInitialRender) {
                                            Flex.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Text.create(statusEnum[info.status]);
                                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(74:27)");
                                        Text.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        if (!isInitialRender) {
                                            Text.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    Text.pop();
                                    Flex.pop();
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Divider.create();
                                        Divider.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(85:25)");
                                        Divider.width(StyleConstants.FULL_WIDTH);
                                        Divider.height({ "id": 134217936, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Divider.backgroundColor({ "id": 134217902, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Divider.margin({
                                            bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Divider.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    {
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            if (isInitialRender) {
                                                ViewPU.create(new CommodityOrderItem(this, { orderData: info }, undefined, elmtId));
                                            }
                                            else {
                                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                    }
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Text.create({ "id": 134217772, "type": 10003, params: [info.orderId], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(94:25)");
                                        Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.width(StyleConstants.FULL_WIDTH);
                                        Text.margin({
                                            top: { "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Text.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    Text.pop();
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Divider.create();
                                        Divider.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(101:25)");
                                        Divider.width(StyleConstants.FULL_WIDTH);
                                        Divider.height({ "id": 134217936, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Divider.backgroundColor({ "id": 134217902, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Divider.margin({
                                            top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Divider.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Flex.create({
                                            direction: FlexDirection.Column,
                                            alignItems: ItemAlign.End,
                                            justifyContent: FlexAlign.End
                                        });
                                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(108:25)");
                                        Flex.height({ "id": 134217792, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Flex.padding({
                                            top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Flex.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Text.create({ "id": 134217764, "type": 10003, params: [info.count,
                                                payEnum[info.status], info.price * info.count], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(113:27)");
                                        Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                        Text.margin({
                                            bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                                        });
                                        if (!isInitialRender) {
                                            Text.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    Text.pop();
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        Flex.create({ justifyContent: FlexAlign.End });
                                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(119:27)");
                                        Flex.width(StyleConstants.FULL_WIDTH);
                                        if (!isInitialRender) {
                                            Flex.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                        If.create();
                                        if (info.status === OrderOperationStatus.UN_PAY) {
                                            this.ifElseBranchUpdateFunction(0, () => {
                                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                                    Button.createWithLabel({ "id": 134217778, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(121:31)");
                                                    Button.height({ "id": 134217791, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.fontColor(Color.White);
                                                    Button.backgroundColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.borderRadius({ "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.onClick(() => {
                                                        this.paymentHandler(info.orderId, OrderOperationStatus.DELIVERED, OrderDetailConstants.ORDER_PAY);
                                                    });
                                                    if (!isInitialRender) {
                                                        Button.pop();
                                                    }
                                                    ViewStackProcessor.StopGetAccessRecording();
                                                });
                                                Button.pop();
                                            });
                                        }
                                        else if (info.status === OrderOperationStatus.DELIVERED) {
                                            this.ifElseBranchUpdateFunction(1, () => {
                                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                                    Button.createWithLabel({ "id": 134217761, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(131:31)");
                                                    Button.height({ "id": 134217791, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.fontColor(Color.White);
                                                    Button.backgroundColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.borderRadius({ "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                                                    Button.onClick(() => {
                                                        this.paymentHandler(info.orderId, OrderOperationStatus.RECEIPT, OrderDetailConstants.ORDER_CONFIRM);
                                                    });
                                                    if (!isInitialRender) {
                                                        Button.pop();
                                                    }
                                                    ViewStackProcessor.StopGetAccessRecording();
                                                });
                                                Button.pop();
                                            });
                                        }
                                        if (!isInitialRender) {
                                            If.pop();
                                        }
                                        ViewStackProcessor.StopGetAccessRecording();
                                    });
                                    If.pop();
                                    Flex.pop();
                                    Flex.pop();
                                    Flex.pop();
                                    ListItem.pop();
                                };
                                if (isLazyCreate) {
                                    observedShallowRender();
                                }
                                else {
                                    observedDeepRender();
                                }
                            }
                        };
                        this.forEachUpdateFunction(elmtId, this.status === OrderOperationStatus.ALLStatus ?
                            this.orderList : this.orderList.filter(item => item.status === this.status), forEachItemGenFunction);
                        if (!isInitialRender) {
                            ForEach.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    ForEach.pop();
                    List.pop();
                });
            }
            else {
                this.ifElseBranchUpdateFunction(1, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Column.create();
                        Column.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(175:17)");
                        Column.width(StyleConstants.FULL_WIDTH);
                        if (!isInitialRender) {
                            Column.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    {
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            if (isInitialRender) {
                                ViewPU.create(new EmptyComponent(this, { outerHeight: StyleConstants.FIFTY_HEIGHT }, undefined, elmtId));
                            }
                            else {
                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                    }
                    Column.pop();
                });
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217765, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderListContent.ets(180:15)");
            Text.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.margin({
                top: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Text.width(StyleConstants.FULL_WIDTH);
            Text.textAlign(TextAlign.Start);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CommodityList(this, {
                        commodityList: this.__commodityList,
                        column: this.currentBreakpoint === BreakpointConstants.BREAKPOINT_SM ?
                            StyleConstants.DISPLAY_TWO : StyleConstants.DISPLAY_THREE,
                        onClickItem: (data) => {
                            router.pushUrl({
                                url: OrderDetailConstants.COMMODITY_DETAIL_PAGE_URL,
                                params: {
                                    id: data.id
                                }
                            }).catch(err => {
                                Logger.error(JSON.stringify(err));
                            });
                        }
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {
                        column: this.currentBreakpoint === BreakpointConstants.BREAKPOINT_SM ?
                            StyleConstants.DISPLAY_TWO : StyleConstants.DISPLAY_THREE
                    });
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
        GridCol.pop();
        GridRow.pop();
        Scroll.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=OrderListContent.js.map