/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@ohos:router';
import { HeaderBar } from '@bundle:com.example.multishopping/phone@orderdetail/ets/components/HeaderBar';
import { OrderListContent } from '@bundle:com.example.multishopping/phone@orderdetail/ets/components/OrderListContent';
import { OrderDetailConstants } from '@bundle:com.example.multishopping/phone@orderdetail/ets/constants/OrderDetailConstants';
import { EmptyComponent, OrderOperationStatus, StyleConstants, BreakpointConstants } from '@bundle:com.example.multishopping/phone@common/index';
export class OrderDetailList extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__currentBreakpoint = this.createStorageProp('currentBreakpoint', 'sm', "currentBreakpoint");
        this.__currentTabIndex = this.initializeConsume("currentTabIndex", "currentTabIndex");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__currentBreakpoint.aboutToBeDeleted();
        this.__currentTabIndex.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get currentBreakpoint() {
        return this.__currentBreakpoint.get();
    }
    set currentBreakpoint(newValue) {
        this.__currentBreakpoint.set(newValue);
    }
    get currentTabIndex() {
        return this.__currentTabIndex.get();
    }
    set currentTabIndex(newValue) {
        this.__currentTabIndex.set(newValue);
    }
    OrderTabs(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(28:5)");
            Column.height(StyleConstants.FULL_HEIGHT);
            Column.width(StyleConstants.FULL_WIDTH);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Tabs.create({
                barPosition: BarPosition.Start,
                index: this.currentTabIndex
            });
            Tabs.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(29:7)");
            Tabs.barHeight({ "id": 134217793, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Tabs.barWidth(this.currentBreakpoint === BreakpointConstants.BREAKPOINT_LG ?
                StyleConstants.SIXTY_WIDTH : StyleConstants.FULL_WIDTH);
            Tabs.barMode(BarMode.Fixed);
            Tabs.margin({
                top: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, bottom: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Tabs.onChange((index) => {
                this.currentTabIndex = index;
            });
            if (!isInitialRender) {
                Tabs.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new OrderListContent(this, {
                                status: OrderOperationStatus.ALLStatus,
                            }, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
            });
            TabContent.tabBar({ builder: () => {
                    this.tabBar.call(this, { "id": 134217757, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderDetailConstants.ALL_INDEX);
                } });
            TabContent.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(33:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new OrderListContent(this, {
                                status: OrderOperationStatus.UN_PAY,
                            }, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
            });
            TabContent.tabBar({ builder: () => {
                    this.tabBar.call(this, { "id": 134217779, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderDetailConstants.PENDING_PAYMENT_INDEX);
                } });
            TabContent.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(40:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Column.create();
                    Column.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(48:11)");
                    Column.width(StyleConstants.FULL_WIDTH);
                    Column.height(StyleConstants.FULL_HEIGHT);
                    if (!isInitialRender) {
                        Column.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new EmptyComponent(this, {}, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
                Column.pop();
            });
            TabContent.tabBar({ builder: () => {
                    this.tabBar.call(this, { "id": 134217784, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderDetailConstants.WAITING_SHIPMENT_INDEX);
                } });
            TabContent.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(47:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new OrderListContent(this, {
                                status: OrderOperationStatus.DELIVERED,
                            }, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
            });
            TabContent.tabBar({ builder: () => {
                    this.tabBar.call(this, { "id": 134217782, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderDetailConstants.WAITING_RECEIVED_INDEX);
                } });
            TabContent.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(56:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new OrderListContent(this, {
                                status: OrderOperationStatus.RECEIPT,
                            }, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
            });
            TabContent.tabBar({ builder: () => {
                    this.tabBar.call(this, { "id": 134217781, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, OrderDetailConstants.WAITING_EVALUATE_INDEX);
                } });
            TabContent.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(63:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        Tabs.pop();
        Column.pop();
    }
    tabBar(text, index, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
            Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(86:5)");
            Flex.width(StyleConstants.FULL_WIDTH);
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(text);
            Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(87:7)");
            Text.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.textAlign(TextAlign.Center);
            Text.fontColor(this.currentTabIndex === index ? { "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } : { "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(this.currentTabIndex === index ? StyleConstants.FONT_WEIGHT_FIVE : StyleConstants.FONT_WEIGHT_FOUR);
            Text.border({
                color: { "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                width: { bottom: this.currentTabIndex === index ? { "id": 134217943, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } : { "id": 134217786, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } }
            });
            Text.height({ "id": 134217794, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Flex.pop();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column });
            Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/OrderDetailList.ets(102:5)");
            Flex.backgroundColor({ "id": 134217905, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new HeaderBar(this, { title: { "id": 134217769, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, onBack: () => {
                            router.back();
                        } }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        this.OrderTabs.bind(this)();
        Flex.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=OrderDetailList.js.map