/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class OrderDetailConstants {
}
/**
 * Index of all tab.
 */
OrderDetailConstants.ALL_INDEX = 0;
/**
 * Index of pending payment tab.
 */
OrderDetailConstants.PENDING_PAYMENT_INDEX = 1;
/**
 * Index of waiting shipment tab.
 */
OrderDetailConstants.WAITING_SHIPMENT_INDEX = 2;
/**
 * Index of waiting received tab.
 */
OrderDetailConstants.WAITING_RECEIVED_INDEX = 3;
/**
 * Index of waiting evaluate tab.
 */
OrderDetailConstants.WAITING_EVALUATE_INDEX = 4;
/**
 * String value of success.
 */
OrderDetailConstants.SUCCESS = '成功';
/**
 * String value of pay.
 */
OrderDetailConstants.ORDER_PAY = '支付';
/**
 * String value of confirm.
 */
OrderDetailConstants.ORDER_CONFIRM = '确认';
/**
 * String value of price.
 */
OrderDetailConstants.FEE_PRICE = '0.00';
/**
 * String format of date.
 */
OrderDetailConstants.DATE_FORMAT = 'yyyy/mm/dd HH:MM:SS';
/**
 * CommodityDetailPage url.
 */
OrderDetailConstants.COMMODITY_DETAIL_PAGE_URL = 'pages/CommodityDetailPage';
/**
 * PayOrderPage url.
 */
OrderDetailConstants.PAY_ORDER_PAGE_URL = 'pages/PayOrderPage';
/**
 * Order status.
 */
OrderDetailConstants.STATUS_ENUM = {
    0: '待支付',
    1: '卖家已发货',
    2: '已完成'
};
/**
 * Describe of order status.
 */
OrderDetailConstants.STATUS_DESC_ENUM = {
    0: '您提交了订单，请等待系统确认',
    1: '您的订单已发货，请耐心等待',
    2: '您的订单已完成，感谢支持~'
};
/**
 * Payment string enum.
 */
OrderDetailConstants.PAYMENT_ENUM = {
    1: '支付',
    2: '确认收货'
};
//# sourceMappingURL=OrderDetailConstants.js.map