/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { StyleConstants } from '@bundle:com.example.multishopping/phone@common/index';
export class CommodityOrderItem extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.orderData = undefined;
        this.onCommodityClick = () => { };
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.orderData !== undefined) {
            this.orderData = params.orderData;
        }
        if (params.onCommodityClick !== undefined) {
            this.onCommodityClick = params.onCommodityClick;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.orderData) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Flex.create();
                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(31:7)");
                        Flex.height({ "id": 134217789, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Flex.margin({ bottom: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                        Flex.onClick(() => this.onCommodityClick());
                        if (!isInitialRender) {
                            Flex.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Image.create($rawfile(this.orderData.image));
                        Image.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(32:9)");
                        Image.height(StyleConstants.FULL_HEIGHT);
                        Image.aspectRatio(1);
                        if (!isInitialRender) {
                            Image.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Flex.create({ direction: FlexDirection.Column });
                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(35:9)");
                        Flex.margin({ left: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                        Flex.flexShrink(StyleConstants.FLEX_SHRINK);
                        Flex.flexGrow(StyleConstants.FLEX_GROW);
                        Flex.flexBasis(StyleConstants.FLEX_BASIC);
                        if (!isInitialRender) {
                            Flex.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Flex.create({
                            justifyContent: FlexAlign.SpaceBetween,
                            alignItems: ItemAlign.Center
                        });
                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(36:11)");
                        Flex.flexShrink(StyleConstants.FLEX_SHRINK);
                        Flex.flexGrow(StyleConstants.FLEX_GROW);
                        Flex.flexBasis(StyleConstants.FLEX_BASIC);
                        if (!isInitialRender) {
                            Flex.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Flex.create();
                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(40:13)");
                        Flex.flexShrink(StyleConstants.FLEX_SHRINK);
                        Flex.flexGrow(StyleConstants.FLEX_GROW);
                        Flex.flexBasis(StyleConstants.FLEX_BASIC);
                        if (!isInitialRender) {
                            Flex.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create({ "id": 134217893, "type": 10003, params: [this.orderData.title, this.orderData.description], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(41:15)");
                        Text.width(StyleConstants.FULL_WIDTH);
                        Text.fontColor(Color.Black);
                        Text.maxLines(StyleConstants.TWO_TEXT_LINE);
                        Text.textOverflow({ overflow: TextOverflow.Ellipsis });
                        Text.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Text.lineHeight({ "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Text.margin({ bottom: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    Flex.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Flex.create({
                            justifyContent: FlexAlign.Center,
                            direction: FlexDirection.Column,
                            alignItems: ItemAlign.End
                        });
                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(52:13)");
                        Flex.padding({ left: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                        if (!isInitialRender) {
                            Flex.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create({ "id": 134217892, "type": 10003, params: [this.orderData.price], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(57:15)");
                        Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    Flex.pop();
                    Flex.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Flex.create({ justifyContent: FlexAlign.SpaceBetween });
                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(64:11)");
                        Flex.width(StyleConstants.FULL_WIDTH);
                        Flex.flexShrink(StyleConstants.FLEX_SHRINK);
                        Flex.flexGrow(StyleConstants.FLEX_GROW);
                        Flex.flexBasis(StyleConstants.FLEX_BASIC);
                        if (!isInitialRender) {
                            Flex.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Flex.create({ direction: FlexDirection.Column });
                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(65:13)");
                        Flex.width(StyleConstants.SEVENTY_HEIGHT);
                        if (!isInitialRender) {
                            Flex.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create(Object.values(this.orderData.specifications).join('，'));
                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(66:15)");
                        Text.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    Flex.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Flex.create({
                            direction: FlexDirection.Column,
                            alignItems: ItemAlign.End
                        });
                        Flex.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(72:13)");
                        Flex.padding({ left: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                        if (!isInitialRender) {
                            Flex.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create(`x${this.orderData.count}`);
                        Text.debugLine("../../../../../features/orderdetail/src/main/ets/components/CommodityOrderItem.ets(76:15)");
                        Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    Flex.pop();
                    Flex.pop();
                    Flex.pop();
                    Flex.pop();
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=CommodityOrderItem.js.map