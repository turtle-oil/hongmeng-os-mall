/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class CommodityConstants {
}
/**
 * Index of home tab.
 */
CommodityConstants.INDEX_HOME = 0;
/**
 * Index of shopCart tab.
 */
CommodityConstants.INDEX_SHOPPING_CART = 2;
/**
 * The stars of rating.
 */
CommodityConstants.RATING_STARS = 5;
/**
 * Text max line value: 2
 */
CommodityConstants.MAX_LINE = 2;
/**
 * String of choose.
 */
CommodityConstants.SPECIAL_CHOOSE = '已选';
/**
 * ConfirmOrderPage url.
 */
CommodityConstants.CONFIRM_ORDER_PAGE_URL = 'pages/ConfirmOrderPage';
//# sourceMappingURL=CommodityConstants.js.map