const barData = [
    {
        icon: { "id": 134217824, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217845, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    },
    {
        icon: { "id": 134217829, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217832, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    },
    {
        icon: { "id": 134217826, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
        text: { "id": 134217840, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
    }
];
const userEvaluate = {
    "title": '用户评价（2万+）',
    "favorable": '98%',
    "evaluate": [
        {
            'userId': '1',
            'userIcon': 'common/ic_person.png',
            'userNumber': '185****1937',
            'rating': 5,
            'desc': '和描述相符合，很喜欢，做工精致，颜色好看，原装正品。'
        },
        {
            'userId': '2',
            'userIcon': 'common/ic_person.png',
            'userNumber': '185****1937',
            'rating': 4,
            'desc': '和描述相符合，很喜欢，做工精致，颜色好看，原装正品。'
        }
    ]
};
const moreEvaluate = [
    {
        'userId': '3',
        'userIcon': 'common/ic_person.png',
        'userNumber': '185****1937',
        'rating': 5,
        'desc': '和描述相符合，很喜欢，做工精致，颜色好看，原装正品。'
    },
    {
        'userId': '4',
        'userIcon': 'common/ic_person.png',
        'userNumber': '185****1937',
        'rating': 5,
        'desc': '和描述相符合，很喜欢，做工精致，颜色好看，原装正品。'
    },
    {
        'userId': '5',
        'userIcon': 'common/ic_person.png',
        'userNumber': '185****1937',
        'rating': 5,
        'desc': '和描述相符合，很喜欢，做工精致，颜色好看，原装正品。'
    },
    {
        'userId': '5',
        'userIcon': 'common/ic_person.png',
        'userNumber': '185****1937',
        'rating': 4,
        'desc': '和描述相符合，很喜欢，做工精致，颜色好看，原装正品。'
    },
    {
        'userId': '6',
        'userIcon': 'common/ic_person.png',
        'userNumber': '185****1937',
        'rating': 5,
        'desc': '和描述相符合，很喜欢，做工精致，颜色好看，原装正品。'
    }
];
const serviceList = ['免运费（请以提交订单时为准）', '店铺发货&售后', '7天无理由退货'];
export { userEvaluate, moreEvaluate, barData, serviceList };
//# sourceMappingURL=CommodityDetailData.js.map