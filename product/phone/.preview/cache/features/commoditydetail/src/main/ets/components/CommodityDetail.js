/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import promptAction from '@ohos:promptAction';
import router from '@ohos:router';
import { FinishType } from '@bundle:com.example.multishopping/phone@commoditydetail/ets/bean/TypeModel';
import { SpecificationDialog } from '@bundle:com.example.multishopping/phone@commoditydetail/ets/components/SpecificationDialog';
import { barData, userEvaluate, moreEvaluate, serviceList } from '@bundle:com.example.multishopping/phone@commoditydetail/ets/viewmodel/CommodityDetailData';
import { CapsuleGroupButton } from '@bundle:com.example.multishopping/phone@commoditydetail/ets/components/CapsuleGroupButton';
import { CommodityConstants } from '@bundle:com.example.multishopping/phone@commoditydetail/ets/constants/CommodityConstants';
import { LocalDataManager, Logger, StyleConstants, GridConstants, CommonDataSource, getID } from '@bundle:com.example.multishopping/phone@common/index';
function __Button__titleButton() {
    Button.backgroundColor({ "id": 134217911, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
    Button.width({ "id": 134217821, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
    Button.height({ "id": 134217821, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
    Button.borderRadius({ "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
}
export class CommodityDetail extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__commodityId = new SynchedPropertySimpleOneWayPU(params.commodityId, this, "commodityId");
        this.__info = new ObservedPropertyObjectPU(undefined, this, "info");
        this.__selectKeys = new ObservedPropertyObjectPU({}, this, "selectKeys");
        this.__count = new ObservedPropertySimplePU(1, this, "count");
        this.__swiperIndex = new ObservedPropertySimplePU(0, this, "swiperIndex");
        this.localDataManager = LocalDataManager.instance();
        this.data = new CommonDataSource(userEvaluate.evaluate);
        this.dialogController = new CustomDialogController({
            builder: () => {
                let jsDialog = new SpecificationDialog(this, {
                    onFinish: (...params) => this.onSpecificationFinish(...params),
                    data: this.__info,
                    count: this.__count,
                    selectTags: this.__selectKeys,
                });
                jsDialog.setController(this.dialogController);
                ViewPU.create(jsDialog);
            },
            autoCancel: true,
            alignment: DialogAlignment.Bottom,
            customStyle: true
        }, this);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.info !== undefined) {
            this.info = params.info;
        }
        if (params.selectKeys !== undefined) {
            this.selectKeys = params.selectKeys;
        }
        if (params.count !== undefined) {
            this.count = params.count;
        }
        if (params.swiperIndex !== undefined) {
            this.swiperIndex = params.swiperIndex;
        }
        if (params.localDataManager !== undefined) {
            this.localDataManager = params.localDataManager;
        }
        if (params.data !== undefined) {
            this.data = params.data;
        }
        if (params.dialogController !== undefined) {
            this.dialogController = params.dialogController;
        }
    }
    updateStateVars(params) {
        this.__commodityId.reset(params.commodityId);
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__commodityId.purgeDependencyOnElmtId(rmElmtId);
        this.__info.purgeDependencyOnElmtId(rmElmtId);
        this.__selectKeys.purgeDependencyOnElmtId(rmElmtId);
        this.__count.purgeDependencyOnElmtId(rmElmtId);
        this.__swiperIndex.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__commodityId.aboutToBeDeleted();
        this.__info.aboutToBeDeleted();
        this.__selectKeys.aboutToBeDeleted();
        this.__count.aboutToBeDeleted();
        this.__swiperIndex.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get commodityId() {
        return this.__commodityId.get();
    }
    set commodityId(newValue) {
        this.__commodityId.set(newValue);
    }
    get info() {
        return this.__info.get();
    }
    set info(newValue) {
        this.__info.set(newValue);
    }
    get selectKeys() {
        return this.__selectKeys.get();
    }
    set selectKeys(newValue) {
        this.__selectKeys.set(newValue);
    }
    get count() {
        return this.__count.get();
    }
    set count(newValue) {
        this.__count.set(newValue);
    }
    get swiperIndex() {
        return this.__swiperIndex.get();
    }
    set swiperIndex(newValue) {
        this.__swiperIndex.set(newValue);
    }
    onSpecificationFinish(type, count, selectKeys) {
        this.count = count;
        this.selectKeys = selectKeys;
        const params = {
            id: getID(),
            commodityId: this.info.id,
            count,
            specifications: this.selectKeys
        };
        switch (type) {
            case FinishType.JOIN_SHOPPING_CART:
                this.localDataManager.insertShopCart(params);
                promptAction.showToast({
                    message: { "id": 134217838, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                });
                break;
            case FinishType.BUY_NOW:
                router.pushUrl({
                    url: CommodityConstants.CONFIRM_ORDER_PAGE_URL,
                    params: { orderList: [Object.assign(Object.assign({}, this.info), params)] }
                }).catch(err => {
                    Logger.error(JSON.stringify(err));
                });
                break;
        }
    }
    bottomBtnClick(type) {
        this.onSpecificationFinish(type, this.count, this.selectKeys);
    }
    aboutToAppear() {
        this.info = this.localDataManager.queryCommodityListById(this.commodityId);
        this.info.specifications.forEach((item) => {
            this.selectKeys[item.id] = item.data[0].value;
        });
    }
    CustomSwiper(payload, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create({ alignContent: Alignment.BottomEnd });
            Stack.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(116:5)");
            Stack.width(StyleConstants.FULL_WIDTH);
            Stack.backgroundColor(Color.White);
            Stack.height({ "id": 134217817, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Swiper.create();
            Swiper.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(117:7)");
            Swiper.onChange((index) => this.swiperIndex = index);
            Swiper.indicator(false);
            Swiper.width(StyleConstants.FULL_WIDTH);
            Swiper.height(StyleConstants.FULL_HEIGHT);
            if (!isInitialRender) {
                Swiper.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = _item => {
                const item = _item;
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Flex.create({ justifyContent: FlexAlign.Center });
                    Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(119:11)");
                    Flex.margin({
                        left: { "id": 134217818, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                        right: { "id": 134217818, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                        top: { "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                        bottom: { "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                    });
                    if (!isInitialRender) {
                        Flex.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Image.create($rawfile(item));
                    Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(120:13)");
                    Image.height(StyleConstants.FULL_HEIGHT);
                    Image.aspectRatio(1);
                    Image.objectFit(ImageFit.Cover);
                    if (!isInitialRender) {
                        Image.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                Flex.pop();
            };
            this.forEachUpdateFunction(elmtId, payload, forEachItemGenFunction, item => JSON.stringify(item), false, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        Swiper.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`${this.swiperIndex + 1}/${payload.length}`);
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(138:7)");
            Text.fontSize({ "id": 134217930, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor(Color.White);
            Text.textAlign(TextAlign.Center);
            Text.width({ "id": 134217820, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.height({ "id": 134217932, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.backgroundColor({ "id": 134217904, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.borderRadius({ "id": 134217819, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.margin({
                right: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Stack.pop();
    }
    TitleBar(payload, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(157:5)");
            Flex.backgroundColor(Color.White);
            Flex.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.padding({
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Flex.margin({
                top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create();
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(158:7)");
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create({ "id": 134217841, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(159:9)");
            Span.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create(`${payload.price}`);
            Span.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(162:9)");
            Span.fontSize({ "id": 134217917, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Span.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(167:7)");
            Row.width(StyleConstants.FULL_WIDTH);
            Row.margin({ top: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217833, "type": 10003, params: [payload.title, payload.description], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(168:9)");
            Text.fontColor(Color.Black);
            Text.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(StyleConstants.FONT_WEIGHT_FOUR);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ justifyContent: FlexAlign.SpaceBetween });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(176:7)");
            Flex.height({ "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.margin({ top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = _item => {
                const item = _item;
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Row.create();
                    Row.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(178:11)");
                    if (!isInitialRender) {
                        Row.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Image.create(item.icon);
                    Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(179:13)");
                    Image.height({ "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Image.width({ "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Image.margin({ right: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                    if (!isInitialRender) {
                        Image.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Text.create(item.text);
                    Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(183:13)");
                    Text.fontSize({ "id": 134217927, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    if (!isInitialRender) {
                        Text.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                Text.pop();
                Row.pop();
            };
            this.forEachUpdateFunction(elmtId, barData, forEachItemGenFunction, item => JSON.stringify(item), false, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        Flex.pop();
        Flex.pop();
    }
    Specification(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(196:5)");
            Row.backgroundColor(Color.White);
            Row.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Row.padding({
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Row.margin({
                top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217831, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(197:7)");
            Text.fontColor(Color.Black);
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(StyleConstants.FONT_WEIGHT_FIVE);
            Text.margin({ right: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ justifyContent: FlexAlign.SpaceBetween });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(202:7)");
            Flex.layoutWeight(StyleConstants.LAYOUT_WEIGHT);
            Flex.onClick(() => this.dialogController.open());
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`${Object.values(ObservedObject.GetRawObject(this.selectKeys)).join('·')}·x${this.count}`);
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(203:9)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217800, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(205:9)");
            Image.height({ "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.width({ "id": 134217937, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Flex.pop();
        Row.pop();
    }
    SpecialService(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(216:5)");
            Column.backgroundColor(Color.White);
            Column.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Column.padding({
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Column.margin({
                top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({
                justifyContent: FlexAlign.SpaceBetween,
                alignItems: ItemAlign.Start
            });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(217:7)");
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217842, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(221:9)");
            Text.fontColor(Color.Black);
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(StyleConstants.FONT_WEIGHT_FIVE);
            Text.margin({ right: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({
                justifyContent: FlexAlign.SpaceBetween,
                alignItems: ItemAlign.Center
            });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(226:9)");
            Flex.layoutWeight(StyleConstants.LAYOUT_WEIGHT);
            Flex.height({ "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.margin({ bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(230:11)");
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217822, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(231:13)");
            Image.width({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.height({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217843, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(234:13)");
            Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.margin({ left: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217800, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(240:11)");
            Image.height({ "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.width({ "id": 134217937, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Flex.pop();
        Flex.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Divider.create();
            Divider.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(249:7)");
            Divider.margin({
                left: { "id": 134217933, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Divider.height({ "id": 134217816, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Divider.backgroundColor({ "id": 134217910, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Divider.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({
                justifyContent: FlexAlign.SpaceBetween,
                alignItems: ItemAlign.Start
            });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(257:7)");
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217844, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(261:9)");
            Text.fontColor(Color.Black);
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(StyleConstants.FONT_WEIGHT_FIVE);
            Text.margin({ right: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(266:9)");
            Flex.layoutWeight(StyleConstants.LAYOUT_WEIGHT);
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = (_item, index) => {
                const item = _item;
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Flex.create({ alignItems: ItemAlign.Center });
                    Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(268:13)");
                    Flex.height({ "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Flex.margin({
                        bottom: index === serviceList.length - 1 ? 0 : { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
                    });
                    if (!isInitialRender) {
                        Flex.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Image.create({ "id": 134217828, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(269:15)");
                    Image.width({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Image.height({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    if (!isInitialRender) {
                        Image.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Text.create(item);
                    Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(272:15)");
                    Text.fontColor(Color.Black);
                    Text.fontSize({ "id": 134217935, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Text.margin({ left: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                    if (!isInitialRender) {
                        Text.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                Text.pop();
                Flex.pop();
            };
            this.forEachUpdateFunction(elmtId, serviceList, forEachItemGenFunction, item => JSON.stringify(item), true, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        Flex.pop();
        Flex.pop();
        Column.pop();
    }
    UserEvaluate(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: StyleConstants.TWELVE_SPACE });
            Column.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(290:5)");
            Column.backgroundColor(Color.White);
            Column.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Column.padding({
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Column.margin({
                top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(291:7)");
            Row.width(StyleConstants.FULL_WIDTH);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(userEvaluate.title);
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(292:9)");
            Text.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(StyleConstants.FONT_WEIGHT_FIVE);
            Text.fontColor(Color.Black);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            Blank.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(296:9)");
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(userEvaluate.favorable);
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(297:9)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor({ "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217834, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(300:9)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217849, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(303:9)");
            Image.objectFit(ImageFit.Contain);
            Image.height({ "id": 134217942, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.width({ "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
        {
            const __lazyForEachItemGenFunction = _item => {
                const item = _item;
                this.Evaluate.bind(this)(item);
            };
            const __lazyForEachItemIdFunc = (item, index) => JSON.stringify(item) + index;
            LazyForEach.create("1", this, this.data, __lazyForEachItemGenFunction, __lazyForEachItemIdFunc);
            LazyForEach.pop();
        }
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217835, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(313:7)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.width({ "id": 134217815, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.height({ "id": 134217813, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.textAlign(TextAlign.Center);
            Text.border({
                width: { "id": 134217936, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                color: { "id": 134217910, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                radius: { "id": 134217814, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Text.onClick(() => {
                this.data.addData(this.data.totalCount(), moreEvaluate);
            });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
    }
    Evaluate(evaluate, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: StyleConstants.TWELVE_SPACE });
            Column.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(331:5)");
            Column.alignItems(HorizontalAlign.Start);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(332:7)");
            Row.width(StyleConstants.FULL_WIDTH);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create($rawfile(evaluate.userIcon));
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(333:9)");
            Image.objectFit(ImageFit.Contain);
            Image.width({ "id": 134217811, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.height({ "id": 134217811, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: StyleConstants.FOUR_SPACE });
            Column.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(337:9)");
            Column.alignItems(HorizontalAlign.Start);
            Column.width(StyleConstants.FULL_WIDTH);
            Column.margin({ left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(evaluate.userNumber);
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(338:11)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontWeight(StyleConstants.FONT_WEIGHT_FIVE);
            Text.fontColor(Color.Black);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Rating.create({ rating: evaluate.rating });
            Rating.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(342:11)");
            Rating.hitTestBehavior(HitTestMode.None);
            Rating.size({
                width: { "id": 134217812, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                height: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Rating.stars(CommodityConstants.RATING_STARS);
            if (!isInitialRender) {
                Rating.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Rating.pop();
        Column.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(evaluate.desc);
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(356:7)");
            Text.fontSize({ "id": 134217929, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor({ "id": 134217900, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
    }
    DetailList(images, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({
                direction: FlexDirection.Column,
                alignItems: ItemAlign.Center
            });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(364:5)");
            Flex.width(StyleConstants.FULL_WIDTH);
            Flex.backgroundColor(Color.White);
            Flex.borderRadius({ "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.padding({
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                bottom: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            Flex.margin({
                top: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                left: { "id": 134217940, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = _item => {
                const image = _item;
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Image.create($rawfile(image));
                    Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(369:9)");
                    Image.width(StyleConstants.FULL_WIDTH);
                    Image.constraintSize({ maxWidth: { "id": 134217807, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
                    Image.objectFit(ImageFit.Cover);
                    if (!isInitialRender) {
                        Image.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
            };
            this.forEachUpdateFunction(elmtId, images, forEachItemGenFunction, image => JSON.stringify(image), false, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        Flex.pop();
    }
    BottomMenu(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ alignItems: ItemAlign.Center });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(380:5)");
            Flex.height({ "id": 134217933, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.width(StyleConstants.FULL_WIDTH);
            Flex.padding({ right: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            Flex.backgroundColor({ "id": 134217905, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(381:7)");
            Row.height(StyleConstants.FULL_HEIGHT);
            Row.margin({ right: { "id": 134217931, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({
                direction: FlexDirection.Column,
                justifyContent: FlexAlign.Center,
                alignItems: ItemAlign.Center
            });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(382:9)");
            Flex.onClick(() => {
                AppStorage.Set('IndexPage', CommodityConstants.INDEX_HOME);
                router.back();
            });
            Flex.height(StyleConstants.FULL_HEIGHT);
            Flex.width({ "id": 134217805, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217825, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(386:11)");
            Image.height({ "id": 134217806, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.width({ "id": 134217806, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217836, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(389:11)");
            Text.fontSize({ "id": 134217927, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.margin({ top: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Flex.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({
                direction: FlexDirection.Column,
                justifyContent: FlexAlign.Center,
                alignItems: ItemAlign.Center
            });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(401:9)");
            Flex.onClick(() => {
                AppStorage.Set('IndexPage', CommodityConstants.INDEX_SHOPPING_CART);
                router.back();
            });
            Flex.height(StyleConstants.FULL_HEIGHT);
            Flex.width({ "id": 134217805, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217823, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(406:11)");
            Image.height({ "id": 134217806, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.width({ "id": 134217806, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 134217732, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(409:11)");
            Text.fontSize({ "id": 134217927, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.fontColor({ "id": 134217906, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Text.margin({ top: { "id": 134217934, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Flex.pop();
        Row.pop();
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CapsuleGroupButton(this, {
                        configs: [{
                                text: { "id": 134217837, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                onClick: () => this.bottomBtnClick(FinishType.JOIN_SHOPPING_CART)
                            }, {
                                text: { "id": 134217830, "type": 10003, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                                onClick: () => this.bottomBtnClick(FinishType.BUY_NOW)
                            }]
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Flex.pop();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create({ alignContent: Alignment.TopStart });
            Stack.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(440:5)");
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(441:7)");
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Scroll.create();
            Scroll.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(442:9)");
            Scroll.flexGrow(StyleConstants.FLEX_GROW);
            if (!isInitialRender) {
                Scroll.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridRow.create({
                columns: {
                    sm: GridConstants.COLUMN_FOUR,
                    md: GridConstants.COLUMN_EIGHT,
                    lg: GridConstants.COLUMN_TWELVE
                },
                gutter: GridConstants.GUTTER_TWELVE
            });
            GridRow.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(443:11)");
            if (!isInitialRender) {
                GridRow.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridCol.create({
                span: {
                    sm: GridConstants.SPAN_FOUR,
                    md: GridConstants.SPAN_EIGHT,
                    lg: GridConstants.SPAN_TWELVE
                }
            });
            GridCol.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(451:13)");
            if (!isInitialRender) {
                GridCol.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.CustomSwiper.bind(this)(this.info.images);
        GridCol.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridCol.create({
                span: {
                    sm: GridConstants.SPAN_FOUR,
                    md: GridConstants.SPAN_EIGHT,
                    lg: GridConstants.SPAN_EIGHT
                },
                offset: { lg: GridConstants.OFFSET_TWO }
            });
            GridCol.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(460:13)");
            if (!isInitialRender) {
                GridCol.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(468:15)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.info) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.TitleBar.bind(this)(ObservedObject.GetRawObject(this.info));
                    this.Specification.bind(this)();
                    this.SpecialService.bind(this)();
                    this.UserEvaluate.bind(this)();
                    this.DetailList.bind(this)(this.info.images);
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        Column.pop();
        GridCol.pop();
        GridRow.pop();
        Scroll.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridRow.create({
                columns: {
                    sm: GridConstants.COLUMN_FOUR,
                    md: GridConstants.COLUMN_EIGHT,
                    lg: GridConstants.COLUMN_TWELVE
                },
                gutter: GridConstants.GUTTER_TWELVE
            });
            GridRow.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(482:9)");
            if (!isInitialRender) {
                GridRow.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            GridCol.create({
                span: {
                    sm: GridConstants.SPAN_FOUR,
                    md: GridConstants.SPAN_EIGHT,
                    lg: GridConstants.SPAN_EIGHT
                },
                offset: { lg: GridConstants.OFFSET_TWO }
            });
            GridCol.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(490:11)");
            if (!isInitialRender) {
                GridCol.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.BottomMenu.bind(this)();
        GridCol.pop();
        GridRow.pop();
        Flex.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceBetween });
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(502:7)");
            Flex.margin({
                left: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                top: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                right: { "id": 134217938, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }
            });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithChild();
            Button.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(503:9)");
            __Button__titleButton();
            Button.onClick(() => router.back());
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217802, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(504:11)");
            Image.height(StyleConstants.FULL_HEIGHT);
            Image.aspectRatio(1);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithChild();
            Button.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(511:9)");
            __Button__titleButton();
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 134217847, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CommodityDetail.ets(512:11)");
            Image.height(StyleConstants.FULL_HEIGHT);
            Image.aspectRatio(1);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        Flex.pop();
        Stack.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=CommodityDetail.js.map