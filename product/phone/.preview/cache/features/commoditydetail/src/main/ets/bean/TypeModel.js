export var FinishType;
(function (FinishType) {
    FinishType[FinishType["JOIN_SHOPPING_CART"] = 0] = "JOIN_SHOPPING_CART";
    FinishType[FinishType["BUY_NOW"] = 1] = "BUY_NOW";
    FinishType[FinishType["CONFIRM"] = 2] = "CONFIRM";
    FinishType[FinishType["CANCEL"] = 3] = "CANCEL";
})(FinishType || (FinishType = {}));
//# sourceMappingURL=TypeModel.js.map