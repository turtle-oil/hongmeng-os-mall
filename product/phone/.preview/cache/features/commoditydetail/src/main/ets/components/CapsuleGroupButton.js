import { StyleConstants } from '@bundle:com.example.multishopping/phone@common/index';
export class CapsuleGroupButton extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.configs = [];
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.configs !== undefined) {
            this.configs = params.configs;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create();
            Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CapsuleGroupButton.ets(30:5)");
            Flex.backgroundImage({ "id": 134217827, "type": 20000, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" }, ImageRepeat.NoRepeat);
            Flex.backgroundImageSize({ width: StyleConstants.HUNDRED_FIFTEEN_WIDTH, height: StyleConstants.FULL_HEIGHT });
            Flex.backgroundImagePosition(Alignment.Center);
            Flex.borderRadius({ "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.width(StyleConstants.FULL_WIDTH);
            Flex.height({ "id": 134217918, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Flex.flexBasis(StyleConstants.FLEX_BASIC);
            Flex.flexGrow(StyleConstants.FLEX_GROW);
            Flex.flexShrink(StyleConstants.FLEX_SHRINK);
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = (_item, index) => {
                const config = _item;
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Flex.create({ justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center });
                    Flex.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CapsuleGroupButton.ets(32:9)");
                    Flex.border({
                        radius: {
                            topLeft: !!index ? 0 : { "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                            bottomLeft: !!index ? 0 : { "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" },
                            topRight: !!index ? { "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } : 0,
                            bottomRight: !!index ? { "id": 134217941, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } : 0
                        }
                    });
                    Flex.onClick((event) => config.onClick(event));
                    if (!isInitialRender) {
                        Flex.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Text.create(config.text);
                    Text.debugLine("../../../../../features/commoditydetail/src/main/ets/components/CapsuleGroupButton.ets(33:11)");
                    Text.fontColor(Color.White);
                    Text.fontSize({ "id": 134217928, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Text.height({ "id": 134217918, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
                    Text.textAlign(TextAlign.Center);
                    if (!isInitialRender) {
                        Text.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                Text.pop();
                Flex.pop();
            };
            this.forEachUpdateFunction(elmtId, this.configs, forEachItemGenFunction, config => JSON.stringify(config), true, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        Flex.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=CapsuleGroupButton.js.map