/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@ohos:router';
import { OrderDetailList } from '@bundle:com.example.multishopping/phone@orderdetail/index';
import { LocalDataManager } from '@bundle:com.example.multishopping/phone@common/index';
import { PageConstants } from '@bundle:com.example.multishopping/phone/ets/constants/PageConstants';
class OrderDetailListPage extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__orderList = new ObservedPropertyObjectPU([], this, "orderList");
        this.addProvidedVar("orderList", this.__orderList);
        this.__commodityList = new ObservedPropertyObjectPU([], this, "commodityList");
        this.addProvidedVar("commodityList", this.__commodityList);
        this.__currentTabIndex = new ObservedPropertySimplePU(0, this, "currentTabIndex");
        this.addProvidedVar("currentTabIndex", this.__currentTabIndex);
        this.localDataManager = LocalDataManager.instance();
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.orderList !== undefined) {
            this.orderList = params.orderList;
        }
        if (params.commodityList !== undefined) {
            this.commodityList = params.commodityList;
        }
        if (params.currentTabIndex !== undefined) {
            this.currentTabIndex = params.currentTabIndex;
        }
        if (params.localDataManager !== undefined) {
            this.localDataManager = params.localDataManager;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__orderList.aboutToBeDeleted();
        this.__commodityList.aboutToBeDeleted();
        this.__currentTabIndex.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get orderList() {
        return this.__orderList.get();
    }
    set orderList(newValue) {
        this.__orderList.set(newValue);
    }
    get commodityList() {
        return this.__commodityList.get();
    }
    set commodityList(newValue) {
        this.__commodityList.set(newValue);
    }
    get currentTabIndex() {
        return this.__currentTabIndex.get();
    }
    set currentTabIndex(newValue) {
        this.__currentTabIndex.set(newValue);
    }
    aboutToAppear() {
        this.currentTabIndex = router.getParams()[PageConstants.TABLE_INDEX_KEY] || 0;
    }
    onPageShow() {
        this.orderList = this.localDataManager.queryOrderList();
        this.commodityList = this.localDataManager.queryCommodityList();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/OrderDetailListPage.ets(39:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new OrderDetailList(this, {}, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new OrderDetailListPage(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=OrderDetailListPage.js.map