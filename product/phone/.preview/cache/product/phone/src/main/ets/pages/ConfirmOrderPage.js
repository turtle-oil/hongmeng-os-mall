/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@ohos:router';
import { ConfirmOrder } from '@bundle:com.example.multishopping/phone@orderdetail/index';
import { Order, Logger } from '@bundle:com.example.multishopping/phone@common/index';
import { PageConstants } from '@bundle:com.example.multishopping/phone/ets/constants/PageConstants';
class ConfirmPageOrder extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__amount = new ObservedPropertySimplePU(0, this, "amount");
        this.addProvidedVar("amount", this.__amount);
        this.__orderList = new ObservedPropertyObjectPU([], this, "orderList");
        this.addProvidedVar("orderList", this.__orderList);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.amount !== undefined) {
            this.amount = params.amount;
        }
        if (params.orderList !== undefined) {
            this.orderList = params.orderList;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__amount.aboutToBeDeleted();
        this.__orderList.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get amount() {
        return this.__amount.get();
    }
    set amount(newValue) {
        this.__amount.set(newValue);
    }
    get orderList() {
        return this.__orderList.get();
    }
    set orderList(newValue) {
        this.__orderList.set(newValue);
    }
    onPageShow() {
        const orderList = router.getParams()[PageConstants.ORDER_LIST_KEY];
        this.orderList = orderList.map(item => new Order({
            orderId: item.id,
            commodityId: item.commodityId,
            price: item.price,
            count: item.count,
            specifications: item.specifications,
            image: item.images[0],
            description: item.description,
            title: item.title
        }));
        Logger.info('onPageShow orderList length: ' + this.orderList.length);
        this.amount = this.orderList.reduce((s, item) => s + item.price * item.count, 0);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/ConfirmOrderPage.ets(44:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new ConfirmOrder(this, {}, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new ConfirmPageOrder(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=ConfirmOrderPage.js.map