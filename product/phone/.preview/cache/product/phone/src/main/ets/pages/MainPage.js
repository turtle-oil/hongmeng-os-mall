/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@ohos:router';
import { BreakpointSystem, LocalDataManager, Logger, OrderType, OrderOperationStatus, StyleConstants, BreakpointConstants } from '@bundle:com.example.multishopping/phone@common/index';
import { Home } from '@bundle:com.example.multishopping/phone@home/index';
import { NewProduct } from '@bundle:com.example.multishopping/phone@newproduct/index';
import { ShopCart } from '@bundle:com.example.multishopping/phone@shopcart/index';
import { Personal } from '@bundle:com.example.multishopping/phone@personal/index';
import { buttonInfo } from '@bundle:com.example.multishopping/phone/ets/viewmodel/MainPageData';
import { PageConstants } from '@bundle:com.example.multishopping/phone/ets/constants/PageConstants';
class MainPage extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__currentBreakpoint = this.createStorageProp('currentBreakpoint', 'sm', "currentBreakpoint");
        this.__currentPageIndex = this.createStorageLink('IndexPage', 0, "currentPageIndex");
        this.__shoppingCartCache = new ObservedPropertyObjectPU([], this, "shoppingCartCache");
        this.__shoppingCartList = new ObservedPropertyObjectPU([], this, "shoppingCartList");
        this.__orderCount = new ObservedPropertyObjectPU({}, this, "orderCount");
        this.breakpointSystem = new BreakpointSystem();
        this.localDataManager = LocalDataManager.instance();
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.shoppingCartCache !== undefined) {
            this.shoppingCartCache = params.shoppingCartCache;
        }
        if (params.shoppingCartList !== undefined) {
            this.shoppingCartList = params.shoppingCartList;
        }
        if (params.orderCount !== undefined) {
            this.orderCount = params.orderCount;
        }
        if (params.breakpointSystem !== undefined) {
            this.breakpointSystem = params.breakpointSystem;
        }
        if (params.localDataManager !== undefined) {
            this.localDataManager = params.localDataManager;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__shoppingCartCache.purgeDependencyOnElmtId(rmElmtId);
        this.__shoppingCartList.purgeDependencyOnElmtId(rmElmtId);
        this.__orderCount.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__currentBreakpoint.aboutToBeDeleted();
        this.__currentPageIndex.aboutToBeDeleted();
        this.__shoppingCartCache.aboutToBeDeleted();
        this.__shoppingCartList.aboutToBeDeleted();
        this.__orderCount.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get currentBreakpoint() {
        return this.__currentBreakpoint.get();
    }
    set currentBreakpoint(newValue) {
        this.__currentBreakpoint.set(newValue);
    }
    get currentPageIndex() {
        return this.__currentPageIndex.get();
    }
    set currentPageIndex(newValue) {
        this.__currentPageIndex.set(newValue);
    }
    get shoppingCartCache() {
        return this.__shoppingCartCache.get();
    }
    set shoppingCartCache(newValue) {
        this.__shoppingCartCache.set(newValue);
    }
    get shoppingCartList() {
        return this.__shoppingCartList.get();
    }
    set shoppingCartList(newValue) {
        this.__shoppingCartList.set(newValue);
    }
    get orderCount() {
        return this.__orderCount.get();
    }
    set orderCount(newValue) {
        this.__orderCount.set(newValue);
    }
    aboutToAppear() {
        var _a;
        this.breakpointSystem.register();
        this.shoppingCartList = ((_a = this.shoppingCartCache) === null || _a === void 0 ? void 0 : _a.length) > 0 ? this.shoppingCartCache : [];
        this.queryOrderList();
    }
    aboutToDisappear() {
        this.breakpointSystem.unregister();
    }
    queryShopCart() {
        const shoppingData = this.localDataManager.queryShopCart();
        this.shoppingCartList = shoppingData;
        this.shoppingCartCache = shoppingData;
    }
    routerDetailPage(data) {
        router.pushUrl({
            url: PageConstants.COMMODITY_DETAIL_PAGE_URL,
            params: { id: data.id }
        }).catch(err => {
            Logger.error(JSON.stringify(err));
        });
    }
    queryOrderList() {
        const orderList = this.localDataManager.queryOrderList();
        this.orderCount = {
            [OrderType.PAYMENT]: orderList.filter(item => item.status === OrderOperationStatus.UN_PAY).length,
            [OrderType.SHIP]: 0,
            [OrderType.RECEIPT]: orderList.filter(item => item.status === OrderOperationStatus.DELIVERED).length,
            [OrderType.EVALUATION]: orderList.filter(item => item.status === OrderOperationStatus.RECEIPT).length,
            [OrderType.SALE]: 0
        };
    }
    onPageShow() {
        this.queryShopCart();
        this.queryOrderList();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/MainPage.ets(88:5)");
            Column.backgroundColor({ "id": 134217905, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Tabs.create({
                barPosition: this.currentBreakpoint === BreakpointConstants.BREAKPOINT_LG ? BarPosition.Start : BarPosition.End,
                index: this.currentPageIndex
            });
            Tabs.debugLine("pages/MainPage.ets(89:7)");
            Tabs.barWidth(this.currentBreakpoint === BreakpointConstants.BREAKPOINT_LG ? { "id": 134217743, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } : StyleConstants.FULL_WIDTH);
            Tabs.barHeight(this.currentBreakpoint === BreakpointConstants.BREAKPOINT_LG ?
                StyleConstants.SIXTY_HEIGHT : { "id": 134217933, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Tabs.vertical(this.currentBreakpoint === BreakpointConstants.BREAKPOINT_LG);
            Tabs.scrollable(false);
            Tabs.onChange((index) => {
                this.currentPageIndex = index;
                if (index === PageConstants.PERSONAL_INDEX) {
                    this.queryShopCart();
                }
                else if (index === PageConstants.PERSONAL_INDEX) {
                    this.queryOrderList();
                }
            });
            if (!isInitialRender) {
                Tabs.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new Home(this, {
                                onClickItem: (data) => this.routerDetailPage(data)
                            }, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
            });
            TabContent.tabBar({ builder: () => {
                    this.BottomNavigation.call(this, buttonInfo[PageConstants.HOME_INDEX]);
                } });
            TabContent.debugLine("pages/MainPage.ets(93:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new NewProduct(this, {}, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
            });
            TabContent.tabBar({ builder: () => {
                    this.BottomNavigation.call(this, buttonInfo[PageConstants.NEW_PRODUCT_INDEX]);
                } });
            TabContent.debugLine("pages/MainPage.ets(100:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new ShopCart(this, {
                                products: this.__shoppingCartList,
                                onNeedUpdate: () => this.queryShopCart()
                            }, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
            });
            TabContent.tabBar({ builder: () => {
                    this.BottomNavigation.call(this, buttonInfo[PageConstants.SHOP_CART_INDEX]);
                } });
            TabContent.debugLine("pages/MainPage.ets(105:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TabContent.create(() => {
                {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        if (isInitialRender) {
                            ViewPU.create(new Personal(this, { orderCount: this.__orderCount }, undefined, elmtId));
                        }
                        else {
                            this.updateStateVarsOfChildByElmtId(elmtId, {});
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                }
            });
            TabContent.tabBar({ builder: () => {
                    this.BottomNavigation.call(this, buttonInfo[PageConstants.PERSONAL_INDEX]);
                } });
            TabContent.debugLine("pages/MainPage.ets(113:9)");
            if (!isInitialRender) {
                TabContent.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TabContent.pop();
        Tabs.pop();
        Column.pop();
    }
    BottomNavigation(button, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: PageConstants.BUTTON_SPACE });
            Column.debugLine("pages/MainPage.ets(137:5)");
            Column.width(StyleConstants.FULL_WIDTH);
            Column.height(StyleConstants.FULL_HEIGHT);
            Column.alignItems(HorizontalAlign.Center);
            Column.justifyContent(FlexAlign.Center);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create(this.currentPageIndex === button.index ? button.selectImg : button.img);
            Image.debugLine("pages/MainPage.ets(138:7)");
            Image.objectFit(ImageFit.Contain);
            Image.width({ "id": 134217744, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            Image.height({ "id": 134217744, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(button.title);
            Text.debugLine("pages/MainPage.ets(142:7)");
            Text.fontColor(this.currentPageIndex === button.index ? { "id": 134217903, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" } : Color.Black);
            Text.opacity(this.currentPageIndex === button.index ? StyleConstants.FULL_OPACITY : StyleConstants.SIXTY_OPACITY);
            Text.fontWeight(StyleConstants.FONT_WEIGHT_FIVE);
            Text.textAlign(TextAlign.Center);
            Text.fontSize({ "id": 134217927, "type": 10002, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new MainPage(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=MainPage.js.map