var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import hilog from '@ohos:hilog';
import UIAbility from '@ohos:app.ability.UIAbility';
import Window from '@ohos:window';
import deviceInfo from '@ohos:deviceInfo';
let EntryAbility = class EntryAbility extends UIAbility {
    onCreate(want, launchParam) {
        var _a, _b;
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onCreate');
        hilog.info(0x0000, 'testTag', '%{public}s', (_a = 'want param:' + JSON.stringify(want)) !== null && _a !== void 0 ? _a : '');
        hilog.info(0x0000, 'testTag', '%{public}s', (_b = 'launchParam:' + JSON.stringify(launchParam)) !== null && _b !== void 0 ? _b : '');
        if (deviceInfo.deviceType !== 'tablet') {
            Window.getLastWindow(this.context, (err, data) => {
                if (err.code) {
                    hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.ERROR);
                    hilog.error(0x0000, 'testTag', 'Failed to obtain the top window. Cause: ' + JSON.stringify(err));
                    return;
                }
                let orientation = Window.Orientation.PORTRAIT;
                data.setPreferredOrientation(orientation, (err) => {
                    if (err.code) {
                        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.ERROR);
                        hilog.error(0x0000, 'testTag', 'Failed to set window orientation. Cause: ' + JSON.stringify(err));
                        return;
                    }
                    hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
                    hilog.info(0x0000, 'testTag', 'Succeeded in setting window orientation.');
                });
            });
        }
    }
    onDestroy() {
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onDestroy');
    }
    onWindowStageCreate(windowStage) {
        // Main window is created, set main page for this ability
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageCreate');
        windowStage.loadContent('pages/SplashPage', (err, data) => {
            var _a, _b;
            if (err.code) {
                hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.ERROR);
                hilog.error(0x0000, 'testTag', 'Failed to load the content. Cause: %{public}s', (_a = JSON.stringify(err)) !== null && _a !== void 0 ? _a : '');
                return;
            }
            hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
            hilog.info(0x0000, 'testTag', 'Succeeded in loading the content. Data: %{public}s', (_b = JSON.stringify(data)) !== null && _b !== void 0 ? _b : '');
        });
    }
    onWindowStageDestroy() {
        // Main window is destroyed, release UI related resources
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageDestroy');
    }
    onForeground() {
        // Ability has brought to foreground
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onForeground');
    }
    onBackground() {
        // Ability has back to background
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onBackground');
    }
};
EntryAbility = __decorate([
    Entry
], EntryAbility);
export default EntryAbility;
//# sourceMappingURL=EntryAbility.js.map