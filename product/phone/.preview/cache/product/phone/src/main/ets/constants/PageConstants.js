/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class PageConstants {
}
/**
 * Delay time to router MainPage.
 */
PageConstants.DELAY_TIME = 2000;
/**
 * The value of letter space: 16vp.
 */
PageConstants.LETTER_SPACE = '16vp';
/**
 * The value of tab bar button space: 6vp.
 */
PageConstants.BUTTON_SPACE = '6vp';
/**
 * MainPage url.
 */
PageConstants.MAIN_PAGE_URL = 'pages/MainPage';
/**
 * CommodityDetailPage url.
 */
PageConstants.COMMODITY_DETAIL_PAGE_URL = 'pages/CommodityDetailPage';
/**
 * Index of home tab.
 */
PageConstants.HOME_INDEX = 0;
/**
 * Index of newProduct tab.
 */
PageConstants.NEW_PRODUCT_INDEX = 1;
/**
 * Index of shopCart tab.
 */
PageConstants.SHOP_CART_INDEX = 2;
/**
 * Index of personal tab.
 */
PageConstants.PERSONAL_INDEX = 3;
/**
 * Image aspect ratio value: 1.
 */
PageConstants.IMAGE_ASPECT_RATIO = 1;
/**
 * Key of router get params: tabIndex.
 */
PageConstants.TABLE_INDEX_KEY = 'tabIndex';
/**
 * Key of router get params: orderList.
 */
PageConstants.ORDER_LIST_KEY = 'orderList';
/**
 * Key of router get params: order.
 */
PageConstants.ORDER_KEY = 'order';
/**
 * Key of router get params: orderIds.
 */
PageConstants.ORDER_IDS_KEY = 'orderIds';
/**
 * Key of router get params: id.
 */
PageConstants.ID_KEY = 'id';
//# sourceMappingURL=PageConstants.js.map