/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@ohos:router';
import { PayOrder } from '@bundle:com.example.multishopping/phone@orderdetail/index';
import { LocalDataManager } from '@bundle:com.example.multishopping/phone@common/index';
import { PageConstants } from '@bundle:com.example.multishopping/phone/ets/constants/PageConstants';
class PayOrderPage extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__data = new ObservedPropertyObjectPU([], this, "data");
        this.addProvidedVar("orderList", this.__data);
        this.addProvidedVar("data", this.__data);
        this.__totalPrice = new ObservedPropertySimplePU(0, this, "totalPrice");
        this.localDataManager = LocalDataManager.instance();
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.data !== undefined) {
            this.data = params.data;
        }
        if (params.totalPrice !== undefined) {
            this.totalPrice = params.totalPrice;
        }
        if (params.localDataManager !== undefined) {
            this.localDataManager = params.localDataManager;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__totalPrice.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__data.aboutToBeDeleted();
        this.__totalPrice.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get data() {
        return this.__data.get();
    }
    set data(newValue) {
        this.__data.set(newValue);
    }
    get totalPrice() {
        return this.__totalPrice.get();
    }
    set totalPrice(newValue) {
        this.__totalPrice.set(newValue);
    }
    aboutToAppear() {
        const order = router.getParams()[PageConstants.ORDER_KEY];
        const orderIds = router.getParams()[PageConstants.ORDER_IDS_KEY];
        if (order) {
            this.data = [order];
        }
        if (orderIds) {
            const orders = this.localDataManager.queryOrderList();
            this.data = orders.filter(order => orderIds.includes(order.orderId));
        }
        this.totalPrice = this.data.reduce((sum, i) => {
            return sum + i.price * i.count;
        }, 0);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/PayOrderPage.ets(44:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new PayOrder(this, {
                        totalPrice: this.__totalPrice
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new PayOrderPage(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=PayOrderPage.js.map