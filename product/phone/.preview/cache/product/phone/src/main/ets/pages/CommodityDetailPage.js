/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@ohos:router';
import { LocalDataManager } from '@bundle:com.example.multishopping/phone@common/index';
import { CommodityDetail } from '@bundle:com.example.multishopping/phone@commoditydetail/index';
import { PageConstants } from '@bundle:com.example.multishopping/phone/ets/constants/PageConstants';
class CommodityDetailPage extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__commodityId = new ObservedPropertySimplePU('', this, "commodityId");
        this.__info = new ObservedPropertyObjectPU(undefined, this, "info");
        this.localDataManager = LocalDataManager.instance();
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.commodityId !== undefined) {
            this.commodityId = params.commodityId;
        }
        if (params.info !== undefined) {
            this.info = params.info;
        }
        if (params.localDataManager !== undefined) {
            this.localDataManager = params.localDataManager;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__commodityId.purgeDependencyOnElmtId(rmElmtId);
        this.__info.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__commodityId.aboutToBeDeleted();
        this.__info.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get commodityId() {
        return this.__commodityId.get();
    }
    set commodityId(newValue) {
        this.__commodityId.set(newValue);
    }
    get info() {
        return this.__info.get();
    }
    set info(newValue) {
        this.__info.set(newValue);
    }
    aboutToAppear() {
        this.commodityId = (router.getParams() || {})[PageConstants.ID_KEY];
        this.info = this.localDataManager.queryCommodityListById(this.commodityId);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("pages/CommodityDetailPage.ets(34:5)");
            Row.backgroundColor({ "id": 134217905, "type": 10001, params: [], "bundleName": "com.example.multishopping", "moduleName": "phone" });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CommodityDetail(this, {
                        commodityId: this.commodityId
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {
                        commodityId: this.commodityId
                    });
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Row.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new CommodityDetailPage(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=CommodityDetailPage.js.map